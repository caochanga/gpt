import request from '@/utils/request'

// 查询openAI使用地址列表
export function listUrl(query) {
  return request({
    url: '/openai/url/list',
    method: 'get',
    params: query
  })
}

// 查询openAI使用地址详细
export function getUrl(id) {
  return request({
    url: '/openai/url/' + id,
    method: 'get'
  })
}

// 新增openAI使用地址
export function addUrl(data) {
  return request({
    url: '/openai/url',
    method: 'post',
    data: data
  })
}

// 修改openAI使用地址
export function updateUrl(data) {
  return request({
    url: '/openai/url',
    method: 'put',
    data: data
  })
}

// 删除openAI使用地址
export function delUrl(id) {
  return request({
    url: '/openai/url/' + id,
    method: 'delete'
  })
}
// 请求openAi
export function chat(query) {
  return request({
    url: '/openai/url/send' ,
    method: 'get',
    params: query
  })
}
