import request from '@/utils/request'

// 查询OpenAI密钥存储列表
export function listKeys(query) {
  return request({
    url: '/openai/keys/list',
    method: 'get',
    params: query
  })
}

// 查询OpenAI密钥存储详细
export function getKeys(id) {
  return request({
    url: '/openai/keys/' + id,
    method: 'get'
  })
}

// 新增OpenAI密钥存储
export function addKeys(data) {
  return request({
    url: '/openai/keys',
    method: 'post',
    data: data
  })
}

// 修改OpenAI密钥存储
export function updateKeys(data) {
  return request({
    url: '/openai/keys',
    method: 'put',
    data: data
  })
}

// 删除OpenAI密钥存储
export function delKeys(id) {
  return request({
    url: '/openai/keys/' + id,
    method: 'delete'
  })
}
