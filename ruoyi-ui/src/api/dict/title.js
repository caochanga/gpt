import request from '@/utils/request'

// 查询关键词标题生成列表
export function listTitle(query) {
  return request({
    url: '/dict/title/list',
    method: 'get',
    params: query
  })
}

// 查询关键词标题生成详细
export function getTitle(id) {
  return request({
    url: '/dict/title/' + id,
    method: 'get'
  })
}

// 新增关键词标题生成
export function addTitle(data) {
  return request({
    url: '/dict/title',
    method: 'post',
    data: data
  })
}

// 修改关键词标题生成
export function updateTitle(data) {
  return request({
    url: '/dict/title',
    method: 'put',
    data: data
  })
}

// 删除关键词标题生成
export function delTitle(id) {
  return request({
    url: '/dict/title/' + id,
    method: 'delete'
  })
}
