import request from '@/utils/request'

// 查询关键词管理列表
export function listCategory(query) {
  return request({
    url: '/dict/category/listVo',
    method: 'get',
    params: query
  })
}
export function listCategoryParentId(query) {
  return request({
    url: '/dict/category/listParentIdVo',
    method: 'get',
    params: query
  })
}

// 查询关键词管理详细
export function getCategory(id) {
  return request({
    url: '/dict/category/' + id,
    method: 'get'
  })
}

// 新增关键词管理
export function addCategory(data) {
  return request({
    url: '/dict/category',
    method: 'post',
    data: data
  })
}
// 校验关键词
export function verifyCategory(data) {
  return request({
    url: '/dict/category/verifyCategory',
    method: 'post',
    data: data
  })
}
// 修改关键词管理
export function updateCategory(data) {
  return request({
    url: '/dict/category',
    method: 'put',
    data: data
  })
}

// 删除关键词管理
export function delCategory(id) {
  return request({
    url: '/dict/category/' + id,
    method: 'delete'
  })
}
