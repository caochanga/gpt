import request from '@/utils/request'

// 查询用户网站管理列表
export function listWpKey(query) {
  return request({
    url: '/wpKey/list',
    method: 'get',
    params: query
  })
}

// 查询用户网站管理详细
export function getWpKey(id) {
  return request({
    url: '/wpKey/' + id,
    method: 'get'
  })
}

// 新增用户网站管理
export function addWpKey(data) {
  return request({
    url: '/wpKey',
    method: 'post',
    data: data
  })
}

// 修改用户网站管理
export function updateWpKey(data) {
  return request({
    url: '/wpKey',
    method: 'put',
    data: data
  })
}

// 删除用户网站管理
export function delWpKey(id) {
  return request({
    url: '/wpKey/' + id,
    method: 'delete'
  })
}
