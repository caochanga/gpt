import request from '@/utils/request'

// 查询存储对话组的列表
export function listGroups(query) {
  return request({
    url: '/wp/groups/list',
    method: 'get',
    params: query
  })
}

// 查询存储对话组的详细
export function getGroups(id) {
  return request({
    url: '/wp/groups/' + id,
    method: 'get'
  })
}

// 新增存储对话组的
export function addGroups(data) {
  return request({
    url: '/wp/groups',
    method: 'post',
    data: data
  })
}

// 修改存储对话组的
export function updateGroups(data) {
  return request({
    url: '/wp/groups',
    method: 'put',
    data: data
  })
}

// 删除存储对话组的
export function delGroups(id) {
  return request({
    url: '/wp/groups/' + id,
    method: 'delete'
  })
}
