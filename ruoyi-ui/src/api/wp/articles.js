import request from '@/utils/request'

// 查询文章列表
export function listArticles(query) {
  return request({
    url: '/wp/articles/list',
    method: 'get',
    params: query
  })
}

// 查询文章详细
export function getArticles(articlesId) {
  return request({
    url: '/wp/articles/' + articlesId,
    method: 'get'
  })
}

// 新增文章
export function addArticles(data) {
  return request({
    url: '/wp/articles',
    method: 'post',
    data: data
  })
}

// 修改文章
export function updateArticles(data) {
  return request({
    url: '/wp/articles',
    method: 'put',
    data: data
  })
}

// 删除文章
export function delArticles(articlesId) {
  return request({
    url: '/wp/articles/' + articlesId,
    method: 'delete'
  })
}
