import request from '@/utils/request'

// 查询wordpress文章分类列表
export function listCategory(query) {
  return request({
    url: '/wp/category/list',
    method: 'get',
    params: query
  })
}

// 查询wordpress文章分类详细
export function getCategory(id) {
  return request({
    url: '/wp/category/' + id,
    method: 'get'
  })
}

// 新增wordpress文章分类
export function addCategory(data) {
  return request({
    url: '/wp/category/addList',
    method: 'post',
    data: data
  })
}

// 修改wordpress文章分类
export function updateCategory(data) {
  return request({
    url: '/wp/category',
    method: 'put',
    data: data
  })
}

// 删除wordpress文章分类
export function delCategory(id) {
  return request({
    url: '/wp/category/' + id,
    method: 'delete'
  })
}
