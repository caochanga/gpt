import request from '@/utils/request'

// 查询存储对话消息的列表
export function listMsg(query) {
  return request({
    url: '/wp/msg/list',
    method: 'get',
    params: query
  })
}

// 查询存储对话消息的详细
export function getMsg(id) {
  return request({
    url: '/wp/msg/' + id,
    method: 'get'
  })
}

// 新增存储对话消息的
export function addMsg(data) {
  return request({
    url: '/wp/msg',
    method: 'post',
    data: data
  })
}

// 修改存储对话消息的
export function updateMsg(data) {
  return request({
    url: '/wp/msg',
    method: 'put',
    data: data
  })
}

// 删除存储对话消息的
export function delMsg(id) {
  return request({
    url: '/wp/msg/' + id,
    method: 'delete'
  })
}
