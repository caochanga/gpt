import request from '@/utils/request'

// 查询存储任务信息的列表
export function listTasks(query) {
  return request({
    url: '/tasks/tasks/list',
    method: 'get',
    params: query
  })
}

// 查询存储任务信息的详细
export function getTasks(id) {
  return request({
    url: '/tasks/tasks/' + id,
    method: 'get'
  })
}

// 新增存储任务信息的
export function addTasks(data) {
  return request({
    url: '/tasks/tasks',
    method: 'post',
    data: data
  })
}

// 新增添加文章
export function wpArticles(data) {
  return request({
    url: '/tasks/tasks/wpArticles',
    method: 'post',
    data: data
  })
}
// 修改存储任务信息的
export function updateTasks(data) {
  return request({
    url: '/tasks/tasks',
    method: 'put',
    data: data
  })
}

// 删除存储任务信息的
export function delTasks(id) {
  return request({
    url: '/tasks/tasks/' + id,
    method: 'delete'
  })
}
