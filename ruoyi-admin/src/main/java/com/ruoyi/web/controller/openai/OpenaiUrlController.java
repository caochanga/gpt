package com.ruoyi.web.controller.openai;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.OpenaiUrl;
import com.ruoyi.system.domain.dto.ChatRequest;
import com.ruoyi.system.openai.RequestOpenAi;
import com.ruoyi.system.service.IOpenaiUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * openAI使用地址Controller
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
@RestController
@RequestMapping("/openai/url")
public class OpenaiUrlController extends BaseController
{
    @Autowired
    private IOpenaiUrlService openaiUrlService;

    /**
     * 查询openAI使用地址列表
     */
    @PreAuthorize("@ss.hasPermi('openai:url:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpenaiUrl openaiUrl)
    {
        startPage();
        List<OpenaiUrl> list = openaiUrlService.selectOpenaiUrlList(openaiUrl);
        return getDataTable(list);
    }

    @Autowired
    RequestOpenAi requestOpenAi;

    @GetMapping("/send")
    public AjaxResult send(ChatRequest chatRequest)
    {
        return success(requestOpenAi.send(chatRequest));
    }

    /**
     * 导出openAI使用地址列表
     */
    @PreAuthorize("@ss.hasPermi('openai:url:export')")
    @Log(title = "openAI使用地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpenaiUrl openaiUrl)
    {
        List<OpenaiUrl> list = openaiUrlService.selectOpenaiUrlList(openaiUrl);
        ExcelUtil<OpenaiUrl> util = new ExcelUtil<OpenaiUrl>(OpenaiUrl.class);
        util.exportExcel(response, list, "openAI使用地址数据");
    }

    /**
     * 获取openAI使用地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('openai:url:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(openaiUrlService.selectOpenaiUrlById(id));
    }

    /**
     * 新增openAI使用地址
     */
    @PreAuthorize("@ss.hasPermi('openai:url:add')")
    @Log(title = "openAI使用地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpenaiUrl openaiUrl)
    {
        return toAjax(openaiUrlService.insertOpenaiUrl(openaiUrl));
    }

    /**
     * 修改openAI使用地址
     */
    @PreAuthorize("@ss.hasPermi('openai:url:edit')")
    @Log(title = "openAI使用地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpenaiUrl openaiUrl)
    {
        return toAjax(openaiUrlService.updateOpenaiUrl(openaiUrl));
    }

    /**
     * 删除openAI使用地址
     */
    @PreAuthorize("@ss.hasPermi('openai:url:remove')")
    @Log(title = "openAI使用地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(openaiUrlService.deleteOpenaiUrlByIds(ids));
    }
}
