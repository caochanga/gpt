package com.ruoyi.web.controller.wp;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wp.domain.WpKeyCategory;
import com.ruoyi.wp.domain.dto.WpKeyCategoryListDto;
import com.ruoyi.wp.service.IWpKeyCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * wordpress文章分类Controller
 *
 * @author ruoyi
 * @date 2023-06-07
 */
@RestController
@RequestMapping("/wp/category")
public class WpKeyCategoryController extends BaseController
{
    @Autowired
    private IWpKeyCategoryService wpKeyCategoryService;

    /**
     * 查询wordpress文章分类列表
     */
    @GetMapping("/list")
    public TableDataInfo list(WpKeyCategory wpKeyCategory)
    {
        startPage();
        List<WpKeyCategory> list = wpKeyCategoryService.selectWpKeyCategoryList(wpKeyCategory);
        return getDataTable(list);
    }

    /**
     * 导出wordpress文章分类列表
     */
    @Log(title = "wordpress文章分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WpKeyCategory wpKeyCategory)
    {
        List<WpKeyCategory> list = wpKeyCategoryService.selectWpKeyCategoryList(wpKeyCategory);
        ExcelUtil<WpKeyCategory> util = new ExcelUtil<WpKeyCategory>(WpKeyCategory.class);
        util.exportExcel(response, list, "wordpress文章分类数据");
    }

    /**
     * 获取wordpress文章分类详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wpKeyCategoryService.selectWpKeyCategoryById(id));
    }

    /**
     * 新增wordpress文章分类
     */
    @Log(title = "wordpress文章分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WpKeyCategory wpKeyCategory)
    {
        return toAjax(wpKeyCategoryService.insertWpKeyCategory(wpKeyCategory));
    }
    /**
     * 新增wordpress文章分类
     */
    @Log(title = "wordpress文章分类", businessType = BusinessType.INSERT)
    @PostMapping("/addList")
    public AjaxResult addList(@RequestBody WpKeyCategoryListDto dto )
    {
        return toAjax(wpKeyCategoryService.addList(dto));
    }
    /**
     * 修改wordpress文章分类
     */
    @Log(title = "wordpress文章分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WpKeyCategory wpKeyCategory)
    {
        return toAjax(wpKeyCategoryService.updateWpKeyCategory(wpKeyCategory));
    }

    /**
     * 删除wordpress文章分类
     */
    @Log(title = "wordpress文章分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wpKeyCategoryService.deleteWpKeyCategoryByIds(ids));
    }
}
