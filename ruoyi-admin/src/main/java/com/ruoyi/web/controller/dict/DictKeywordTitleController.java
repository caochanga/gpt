package com.ruoyi.web.controller.dict;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.DictKeywordTitle;
import com.ruoyi.system.domain.vo.DictKeywordTitleVo;
import com.ruoyi.system.service.IDictKeywordTitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 关键词标题生成Controller
 * 
 * @author xialang
 * @date 2023-05-30
 */
@RestController
@RequestMapping("/dict/title")
public class DictKeywordTitleController extends BaseController
{
    @Autowired
    private IDictKeywordTitleService dictKeywordTitleService;

    /**
     * 查询关键词标题生成列表
     */
    @PreAuthorize("@ss.hasPermi('dict:title:list')")
    @GetMapping("/list")
    public TableDataInfo list(DictKeywordTitle dictKeywordTitle)
    {
        startPage();
        List<DictKeywordTitle> list = dictKeywordTitleService.selectDictKeywordTitleList(dictKeywordTitle);
        return getDataTable(list);
    }

    /**
     * 导出关键词标题生成列表
     */
    @PreAuthorize("@ss.hasPermi('dict:title:export')")
    @Log(title = "关键词标题生成", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DictKeywordTitle dictKeywordTitle)
    {
        List<DictKeywordTitle> list = dictKeywordTitleService.selectDictKeywordTitleList(dictKeywordTitle);
        ExcelUtil<DictKeywordTitle> util = new ExcelUtil<DictKeywordTitle>(DictKeywordTitle.class);
        util.exportExcel(response, list, "关键词标题生成数据");
    }

    /**
     * 获取关键词标题生成详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:title:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dictKeywordTitleService.selectDictKeywordTitleById(id));
    }

    /**
     * 新增关键词标题生成
     */
    @PreAuthorize("@ss.hasPermi('dict:title:add')")
    @Log(title = "关键词标题生成", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DictKeywordTitle dictKeywordTitle)
    {
        return toAjax(dictKeywordTitleService.insertDictKeywordTitle(dictKeywordTitle));
    }

    /**
     * 修改关键词标题生成
     */
    @PreAuthorize("@ss.hasPermi('dict:title:edit')")
    @Log(title = "关键词标题生成", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DictKeywordTitle dictKeywordTitle)
    {
        return toAjax(dictKeywordTitleService.updateDictKeywordTitle(dictKeywordTitle));
    }

    /**
     * 删除关键词标题生成
     */
    @PreAuthorize("@ss.hasPermi('dict:title:remove')")
    @Log(title = "关键词标题生成", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dictKeywordTitleService.deleteDictKeywordTitleByIds(ids));
    }
}
