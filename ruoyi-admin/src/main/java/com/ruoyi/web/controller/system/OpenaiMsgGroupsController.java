package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OpenaiMsgGroups;
import com.ruoyi.system.service.IOpenaiMsgGroupsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 存储对话组的Controller
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/wp/groups")
public class OpenaiMsgGroupsController extends BaseController
{
    @Autowired
    private IOpenaiMsgGroupsService openaiMsgGroupsService;

    /**
     * 查询存储对话组的列表
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpenaiMsgGroups openaiMsgGroups)
    {
        startPage();
        List<OpenaiMsgGroups> list = openaiMsgGroupsService.selectOpenaiMsgGroupsList(openaiMsgGroups);
        return getDataTable(list);
    }

    /**
     * 导出存储对话组的列表
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:export')")
    @Log(title = "存储对话组的", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpenaiMsgGroups openaiMsgGroups)
    {
        List<OpenaiMsgGroups> list = openaiMsgGroupsService.selectOpenaiMsgGroupsList(openaiMsgGroups);
        ExcelUtil<OpenaiMsgGroups> util = new ExcelUtil<OpenaiMsgGroups>(OpenaiMsgGroups.class);
        util.exportExcel(response, list, "存储对话组的数据");
    }

    /**
     * 获取存储对话组的详细信息
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(openaiMsgGroupsService.selectOpenaiMsgGroupsById(id));
    }

    /**
     * 新增存储对话组的
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:add')")
    @Log(title = "存储对话组的", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpenaiMsgGroups openaiMsgGroups)
    {
        return toAjax(openaiMsgGroupsService.insertOpenaiMsgGroups(openaiMsgGroups));
    }

    /**
     * 修改存储对话组的
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:edit')")
    @Log(title = "存储对话组的", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpenaiMsgGroups openaiMsgGroups)
    {
        return toAjax(openaiMsgGroupsService.updateOpenaiMsgGroups(openaiMsgGroups));
    }

    /**
     * 删除存储对话组的
     */
    @PreAuthorize("@ss.hasPermi('wp:groups:remove')")
    @Log(title = "存储对话组的", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(openaiMsgGroupsService.deleteOpenaiMsgGroupsByIds(ids));
    }
}
