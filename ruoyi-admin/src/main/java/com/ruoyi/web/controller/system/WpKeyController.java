package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.WpKey;
import com.ruoyi.system.service.IWpKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户网站管理Controller
 * 
 * @author ruoyi
 * @date 2023-05-26
 */
@RestController
@RequestMapping("/wpKey")
public class WpKeyController extends BaseController
{
    @Autowired
    private IWpKeyService wpKeyService;

    /**
     * 查询用户网站管理列表
     */
    @PreAuthorize("@ss.hasPermi('wpKey:list')")
    @GetMapping("/list")
    public TableDataInfo list(WpKey wpKey)
    {
        startPage();
        List<WpKey> list = wpKeyService.selectWpKeyList(wpKey);
        return getDataTable(list);
    }

    /**
     * 导出用户网站管理列表
     */
    @PreAuthorize("@ss.hasPermi('wpKey:export')")
    @Log(title = "用户网站管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WpKey wpKey)
    {
        List<WpKey> list = wpKeyService.selectWpKeyList(wpKey);
        ExcelUtil<WpKey> util = new ExcelUtil<WpKey>(WpKey.class);
        util.exportExcel(response, list, "用户网站管理数据");
    }

    /**
     * 获取用户网站管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('wpKey:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wpKeyService.selectWpKeyById(id));
    }

    /**
     * 新增用户网站管理
     */
    @PreAuthorize("@ss.hasPermi('wpKey:add')")
    @Log(title = "用户网站管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WpKey wpKey)
    {
        wpKey.setUserId(SecurityUtils.getUserId());
        return toAjax(wpKeyService.insertWpKey(wpKey));
    }

    /**
     * 修改用户网站管理
     */
    @PreAuthorize("@ss.hasPermi('wpKey:edit')")
    @Log(title = "用户网站管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WpKey wpKey)
    {
        return toAjax(wpKeyService.updateWpKey(wpKey));
    }

    /**
     * 删除用户网站管理
     */
    @PreAuthorize("@ss.hasPermi('wpKey:remove')")
    @Log(title = "用户网站管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wpKeyService.deleteWpKeyByIds(ids));
    }
}
