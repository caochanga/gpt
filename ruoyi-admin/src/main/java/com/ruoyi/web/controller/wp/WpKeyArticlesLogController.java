package com.ruoyi.web.controller.wp;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wp.domain.WpKeyArticlesLog;
import com.ruoyi.wp.service.IWpKeyArticlesLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * PHP文章发布表Controller
 *
 * @author xialang
 * @date 2023-06-09
 */
@RestController
@RequestMapping("/wp/keyArticlesLog")
public class WpKeyArticlesLogController  extends BaseController
{
    @Autowired
    private IWpKeyArticlesLogService wpKeyArticlesLogService;

    /**
     * 查询PHP文章发布表列表
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(WpKeyArticlesLog wpKeyArticlesLog)
    {
        startPage();
        List<WpKeyArticlesLog> list = wpKeyArticlesLogService.selectWpKeyArticlesLogList(wpKeyArticlesLog);
        return getDataTable(list);
    }

    /**
     * 导出PHP文章发布表列表
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:export')")
    @Log(title = "PHP文章发布表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WpKeyArticlesLog wpKeyArticlesLog)
    {
        List<WpKeyArticlesLog> list = wpKeyArticlesLogService.selectWpKeyArticlesLogList(wpKeyArticlesLog);
        ExcelUtil<WpKeyArticlesLog> util = new ExcelUtil<WpKeyArticlesLog>(WpKeyArticlesLog.class);
        util.exportExcel(response, list, "PHP文章发布表数据");
    }

    /**
     * 获取PHP文章发布表详细信息
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wpKeyArticlesLogService.selectWpKeyArticlesLogById(id));
    }

    /**
     * 新增PHP文章发布表
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:add')")
    @Log(title = "PHP文章发布表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WpKeyArticlesLog wpKeyArticlesLog)
    {
        return toAjax(wpKeyArticlesLogService.insertWpKeyArticlesLog(wpKeyArticlesLog));
    }

    /**
     * 修改PHP文章发布表
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:edit')")
    @Log(title = "PHP文章发布表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WpKeyArticlesLog wpKeyArticlesLog)
    {
        return toAjax(wpKeyArticlesLogService.updateWpKeyArticlesLog(wpKeyArticlesLog));
    }

    /**
     * 删除PHP文章发布表
     */
    @PreAuthorize("@ss.hasPermi('wp:keyArticlesLog:remove')")
    @Log(title = "PHP文章发布表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wpKeyArticlesLogService.deleteWpKeyArticlesLogByIds(ids));
    }
}
