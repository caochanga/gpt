package com.ruoyi.web.controller.wp;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wp.domain.WpArticles;
import com.ruoyi.wp.service.IWpArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 文章Controller
 * 
 * @author xialang
 * @date 2023-06-05
 */
@RestController
@RequestMapping("/wp/articles")
public class WpArticlesController extends BaseController
{
    @Autowired
    private IWpArticlesService wpArticlesService;

    /**
     * 查询文章列表
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:list')")
    @GetMapping("/list")
    public TableDataInfo list(WpArticles wpArticles)
    {
        startPage();
        List<WpArticles> list = wpArticlesService.selectWpArticlesList(wpArticles);
        return getDataTable(list);
    }

    /**
     * 导出文章列表
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:export')")
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WpArticles wpArticles)
    {
        List<WpArticles> list = wpArticlesService.selectWpArticlesList(wpArticles);
        ExcelUtil<WpArticles> util = new ExcelUtil<WpArticles>(WpArticles.class);
        util.exportExcel(response, list, "文章数据");
    }

    /**
     * 获取文章详细信息
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:query')")
    @GetMapping(value = "/{articlesId}")
    public AjaxResult getInfo(@PathVariable("articlesId") Long articlesId)
    {
        return success(wpArticlesService.selectWpArticlesByArticlesId(articlesId));
    }

    /**
     * 新增文章
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:add')")
    @Log(title = "文章", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WpArticles wpArticles)
    {
        return toAjax(wpArticlesService.insertWpArticles(wpArticles));
    }

    /**
     * 修改文章
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:edit')")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WpArticles wpArticles)
    {
        return toAjax(wpArticlesService.updateWpArticles(wpArticles));
    }

    /**
     * 删除文章
     */
    @PreAuthorize("@ss.hasPermi('wp:articles:remove')")
    @Log(title = "文章", businessType = BusinessType.DELETE)
	@DeleteMapping("/{articlesIds}")
    public AjaxResult remove(@PathVariable Long[] articlesIds)
    {
        return toAjax(wpArticlesService.deleteWpArticlesByArticlesIds(articlesIds));
    }
}
