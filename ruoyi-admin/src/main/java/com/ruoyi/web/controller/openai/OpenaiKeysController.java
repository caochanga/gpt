package com.ruoyi.web.controller.openai;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.OpenaiKeys;
import com.ruoyi.system.service.IOpenaiKeysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * OpenAI密钥存储Controller
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
@RestController
@RequestMapping("/openai/keys")
public class OpenaiKeysController extends BaseController
{
    @Autowired
    private IOpenaiKeysService openaiKeysService;

    /**
     * 查询OpenAI密钥存储列表
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpenaiKeys openaiKeys)
    {
        startPage();
        List<OpenaiKeys> list = openaiKeysService.selectOpenaiKeysList(openaiKeys);
        return getDataTable(list);
    }

    /**
     * 导出OpenAI密钥存储列表
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:export')")
    @Log(title = "OpenAI密钥存储", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpenaiKeys openaiKeys)
    {
        List<OpenaiKeys> list = openaiKeysService.selectOpenaiKeysList(openaiKeys);
        ExcelUtil<OpenaiKeys> util = new ExcelUtil<OpenaiKeys>(OpenaiKeys.class);
        util.exportExcel(response, list, "OpenAI密钥存储数据");
    }

    /**
     * 获取OpenAI密钥存储详细信息
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(openaiKeysService.selectOpenaiKeysById(id));
    }

    /**
     * 新增OpenAI密钥存储
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:add')")
    @Log(title = "OpenAI密钥存储", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpenaiKeys openaiKeys)
    {
        openaiKeys.setUserId(SecurityUtils.getUserId());
        return toAjax(openaiKeysService.insertOpenaiKeys(openaiKeys));
    }

    /**
     * 修改OpenAI密钥存储
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:edit')")
    @Log(title = "OpenAI密钥存储", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpenaiKeys openaiKeys)
    {
        return toAjax(openaiKeysService.updateOpenaiKeys(openaiKeys));
    }

    /**
     * 删除OpenAI密钥存储
     */
    @PreAuthorize("@ss.hasPermi('openai:keys:remove')")
    @Log(title = "OpenAI密钥存储", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(openaiKeysService.deleteOpenaiKeysByIds(ids));
    }
}
