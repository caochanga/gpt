package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.OpenaiMsg;
import com.ruoyi.system.service.IOpenaiMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 存储对话消息的Controller
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/wp/msg")
public class OpenaiMsgController extends BaseController
{
    @Autowired
    private IOpenaiMsgService openaiMsgService;

    /**
     * 查询存储对话消息的列表
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpenaiMsg openaiMsg)
    {
        startPage();
        List<OpenaiMsg> list = openaiMsgService.selectOpenaiMsgList(openaiMsg);
        return getDataTable(list);
    }

    /**
     * 导出存储对话消息的列表
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:export')")
    @Log(title = "存储对话消息的", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpenaiMsg openaiMsg)
    {
        List<OpenaiMsg> list = openaiMsgService.selectOpenaiMsgList(openaiMsg);
        ExcelUtil<OpenaiMsg> util = new ExcelUtil<OpenaiMsg>(OpenaiMsg.class);
        util.exportExcel(response, list, "存储对话消息的数据");
    }

    /**
     * 获取存储对话消息的详细信息
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(openaiMsgService.selectOpenaiMsgById(id));
    }

    /**
     * 新增存储对话消息的
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:add')")
    @Log(title = "存储对话消息的", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpenaiMsg openaiMsg)
    {
        return toAjax(openaiMsgService.insertOpenaiMsg(openaiMsg));
    }

    /**
     * 修改存储对话消息的
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:edit')")
    @Log(title = "存储对话消息的", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpenaiMsg openaiMsg)
    {
        return toAjax(openaiMsgService.updateOpenaiMsg(openaiMsg));
    }

    /**
     * 删除存储对话消息的
     */
    @PreAuthorize("@ss.hasPermi('wp:msg:remove')")
    @Log(title = "存储对话消息的", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(openaiMsgService.deleteOpenaiMsgByIds(ids));
    }
}
