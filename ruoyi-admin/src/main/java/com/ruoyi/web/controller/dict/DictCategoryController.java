package com.ruoyi.web.controller.dict;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.DictCategory;
import com.ruoyi.system.domain.vo.DictCategoryVo;
import com.ruoyi.system.service.IDictCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 关键词管理Controller
 * 
 * @author xialang
 * @date 2023-05-30
 */
@RestController
@RequestMapping("/dict/category")
public class DictCategoryController extends BaseController
{
    @Autowired
    private IDictCategoryService dictCategoryService;

    /**
     * 查询关键词管理列表
     */
    @PreAuthorize("@ss.hasPermi('dict:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(DictCategory dictCategory)
    {
        startPage();
        List<DictCategory> list = dictCategoryService.selectDictCategoryList(dictCategory);
        return getDataTable(list);
    }

    /**
     * 查询关键词标题生成列表
     */
    @PreAuthorize("@ss.hasPermi('dict:title:listVo')")
    @GetMapping("/listVo")
    public TableDataInfo listVo(DictCategory dictCategory)
    {
        startPage();
        List<DictCategoryVo> list = dictCategoryService.selectDictCategoryListVo(dictCategory);
        return getDataTable(list);
    }
    /**
     * 查询关键词标题生成列表
     */
    @PreAuthorize("@ss.hasPermi('dict:title:listParentIdVo')")
    @GetMapping("/listParentIdVo")
    public TableDataInfo listParentIdVo(DictCategory dictCategory)
    {
        dictCategory.setUserId(SecurityUtils.getUserId());
        List<DictCategoryVo> list = dictCategoryService.listParentIdVo(dictCategory);
        return getDataTable(list);
    }
    /**
     * 导出关键词管理列表
     */
    @PreAuthorize("@ss.hasPermi('dict:category:export')")
    @Log(title = "关键词管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DictCategory dictCategory)
    {
        List<DictCategory> list = dictCategoryService.selectDictCategoryList(dictCategory);
        ExcelUtil<DictCategory> util = new ExcelUtil<DictCategory>(DictCategory.class);
        util.exportExcel(response, list, "关键词管理数据");
    }

    /**
     * 获取关键词管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dictCategoryService.selectDictCategoryById(id));
    }

    /**
     * 新增关键词管理
     */
    @PreAuthorize("@ss.hasPermi('dict:category:add')")
    @Log(title = "关键词管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DictCategory dictCategory)
    {
        return toAjax(dictCategoryService.insertDictCategory(dictCategory));
    }
    /**
     * 校验关键词
     */
    @Log(title = "校验关键词")
    @PostMapping("/verifyCategory")
    public AjaxResult verifyCategory(@RequestBody DictCategory dictCategory)
    {
        Integer count = dictCategoryService.verifyCategory(dictCategory);
        return result(HttpStatus.SUCCESS,"数据查询成功",count);
    }

    /**
     * 修改关键词查询
     */
    @PreAuthorize("@ss.hasPermi('dict:category:edit')")
    @Log(title = "关键词管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DictCategory dictCategory)
    {
        return toAjax(dictCategoryService.updateDictCategory(dictCategory));
    }

    /**
     * 删除关键词管理
     */
    @PreAuthorize("@ss.hasPermi('dict:category:remove')")
    @Log(title = "关键词管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dictCategoryService.deleteDictCategoryByIds(ids));
    }
}
