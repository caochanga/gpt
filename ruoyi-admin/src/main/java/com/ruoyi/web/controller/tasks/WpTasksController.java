package com.ruoyi.web.controller.tasks;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tasks.domain.WpTasks;
import com.ruoyi.tasks.service.IWpTasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 存储任务信息的Controller
 *
 * @author xialang
 * @date 2023-06-01
 */
@RestController
@RequestMapping("/tasks/tasks")
public class WpTasksController extends BaseController
{
    @Autowired
    private IWpTasksService wpTasksService;

    /**
     * 查询存储任务信息的列表
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:list')")
    @GetMapping("/list")
    public TableDataInfo list(WpTasks wpTasks)
    {
        startPage();
        List<WpTasks> list = wpTasksService.selectWpTasksList(wpTasks);
        return getDataTable(list);
    }

    /**
     * 导出存储任务信息的列表
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:export')")
    @Log(title = "存储任务信息的", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WpTasks wpTasks)
    {
        List<WpTasks> list = wpTasksService.selectWpTasksList(wpTasks);
        ExcelUtil<WpTasks> util = new ExcelUtil<WpTasks>(WpTasks.class);
        util.exportExcel(response, list, "存储任务信息的数据");
    }

    /**
     * 获取存储任务信息的详细信息
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(wpTasksService.selectWpTasksById(id));
    }

    /**
     * 新增存储任务信息的
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:wpArticles')")
    @Log(title = "存储任务信息的", businessType = BusinessType.INSERT)
    @PostMapping("/wpArticles")
    public AjaxResult tasksArticlesAdd(@RequestBody WpTasks wpTasks)
    {
        return toAjax(wpTasksService.tasksArticlesAdd(wpTasks));
    }

    /**
     * 新增存储文章任务信息的
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:add')")
    @Log(title = "存储任务信息的", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WpTasks wpTasks)
    {
        return toAjax(wpTasksService.insertWpTasks(wpTasks));
    }

    /**
     * 修改存储任务信息的
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:edit')")
    @Log(title = "存储任务信息的", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WpTasks wpTasks)
    {
        return toAjax(wpTasksService.updateWpTasks(wpTasks));
    }

    /**
     * 删除存储任务信息的
     */
    @PreAuthorize("@ss.hasPermi('tasks:tasks:remove')")
    @Log(title = "存储任务信息的", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wpTasksService.deleteWpTasksByIds(ids));
    }
}
