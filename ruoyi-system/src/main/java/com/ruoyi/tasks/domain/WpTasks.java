package com.ruoyi.tasks.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 存储任务信息的对象 wp_tasks
 *
 * @author xialang
 * @date 2023-06-01
 */
public class WpTasks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 任务的唯一标识符 */
    private Integer id;

    /** 提交任务的用户ID */
    @Excel(name = "提交任务的用户ID")
    private Long userId;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String tasksName;

    /** 总重复次数 */
    @Excel(name = "总重复次数")
    private Integer totalRepetitions;

    /** 每次请求的重复次数 */
    @Excel(name = "每次请求的重复次数")
    private Integer repetitionsPerRequest;

    /** 已完成的重复次数 */
    @Excel(name = "已完成的重复次数")
    private Integer repetitionsCompleted;

    /** 任务状态（1：未执行，2、执行中、3、掉单、4、已完成) */
    @Excel(name = "任务状态", readConverterExp = "任务状态（1：未执行，2、执行中、3、掉单、4、已完成)")
    private Integer status;

    /** 任务类型(1、写标题，2、写文章，3、用户自定义，0、其他) */
    private Integer tasksType;

    /** 运行的分组请求名称 */
    @Excel(name = "运行的分组请求名称")
    private Long openaiMsgGroupsId;
    /** 活动参数值，如果是写标题，那就是dict_category 父级Id */
    private Long tasksOption;

    public Long getTasksOption() {
        return tasksOption;
    }

    public void setTasksOption(Long tasksOption) {
        this.tasksOption = tasksOption;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTasksName(String tasksName)
    {
        this.tasksName = tasksName;
    }

    public String getTasksName()
    {
        return tasksName;
    }
    public void setTotalRepetitions(Integer totalRepetitions)
    {
        this.totalRepetitions = totalRepetitions;
    }

    public Integer getTotalRepetitions()
    {
        return totalRepetitions;
    }
    public void setRepetitionsPerRequest(Integer repetitionsPerRequest)
    {
        this.repetitionsPerRequest = repetitionsPerRequest;
    }

    public Integer getRepetitionsPerRequest()
    {
        return repetitionsPerRequest;
    }
    public void setRepetitionsCompleted(Integer repetitionsCompleted)
    {
        this.repetitionsCompleted = repetitionsCompleted;
    }

    public Integer getRepetitionsCompleted()
    {
        return repetitionsCompleted;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setTasksType(Integer tasksType)
    {
        this.tasksType = tasksType;
    }

    public Integer getTasksType()
    {
        return tasksType;
    }
    public void setOpenaiMsgGroupsId(Long openaiMsgGroupsId)
    {
        this.openaiMsgGroupsId = openaiMsgGroupsId;
    }

    public Long getOpenaiMsgGroupsId()
    {
        return openaiMsgGroupsId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("tasksName", getTasksName())
                .append("totalRepetitions", getTotalRepetitions())
                .append("repetitionsPerRequest", getRepetitionsPerRequest())
                .append("repetitionsCompleted", getRepetitionsCompleted())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("tasksType", getTasksType())
                .append("openaiMsgGroupsId", getOpenaiMsgGroupsId())
                .toString();
    }
}
