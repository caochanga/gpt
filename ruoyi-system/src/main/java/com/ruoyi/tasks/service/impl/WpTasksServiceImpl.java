package com.ruoyi.tasks.service.impl;

import cn.hutool.json.JSONObject;
import com.ruoyi.common.enums.wp.WPTasksType;
import com.ruoyi.common.enums.wp.WpTasksStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.rabbit.service.RabbitSenderService;
import com.ruoyi.system.domain.DictKeywordTitle;
import com.ruoyi.system.domain.OpenaiMsg;
import com.ruoyi.system.service.IDictKeywordTitleService;
import com.ruoyi.system.service.IOpenaiMsgService;
import com.ruoyi.tasks.domain.WpTasks;
import com.ruoyi.tasks.mapper.WpTasksMapper;
import com.ruoyi.tasks.service.IWpTasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 存储任务信息的Service业务层处理
 *
 * @author xialang
 * @date 2023-06-01
 */
@Service
public class WpTasksServiceImpl implements IWpTasksService
{
    @Autowired
    private WpTasksMapper wpTasksMapper;
    @Autowired
    @Qualifier("rabbitSenderService")
    RabbitSenderService rabbitSenderService;
    @Autowired
    IOpenaiMsgService iOpenaiMsgService;
    @Autowired
    IDictKeywordTitleService iDictKeywordTitleService;

    /**
     * 查询存储任务信息的
     *
     * @param id 存储任务信息的主键
     * @return 存储任务信息的
     */
    @Override
    public WpTasks selectWpTasksById(Integer id)
    {
        return wpTasksMapper.selectWpTasksById(id);
    }

    /**
     * 查询存储任务信息的列表
     *
     * @param wpTasks 存储任务信息的
     * @return 存储任务信息的
     */
    @Override
    public List<WpTasks> selectWpTasksList(WpTasks wpTasks)
    {
        return wpTasksMapper.selectWpTasksList(wpTasks);
    }

    /**
     * 新增存储任务信息的
     *
     * @param wpTasks 存储任务信息的
     * @return 结果
     */
    @Override
    public int insertWpTasks(WpTasks wpTasks)
    {

        if(wpTasks.getTotalRepetitions()>11){
            throw new ServiceException("总次数不能大于10！可以分多次请求！");
        }
        if(wpTasks.getTasksType()==null){
            throw new ServiceException("任务类型参数不能为空！");
        }
        if(wpTasks.getRepetitionsCompleted()==null || wpTasks.getRepetitionsCompleted()==0){
            wpTasks.setRepetitionsCompleted(1);
        }
        wpTasks.setCreateTime(DateUtils.getNowDate());
        wpTasks.setUserId(SecurityUtils.getUserId());
        wpTasks.setStatus(WpTasksStatus.TO_EXECUTE.getCode());
        if(!WPTasksType.ADD_TITLE.getCode().equals(wpTasks.getTasksType())){
            throw new ServiceException("tasksType参数错误！请检查参数是否正确！");
        }
        if(wpTasks.getTasksOption()==null||wpTasks.getTasksOption()<0){
            throw new ServiceException("添加标题，参数值tasksOption不能为空！");
        }
        int i = wpTasksMapper.insertWpTasks(wpTasks);

        System.out.println("数据库参数添加成功，发送MQ执行中...");
        System.out.println(i);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",WpTasksStatus.TO_EXECUTE.getCode());
        jsonObject.put("wpTasks",wpTasks);
        String toString = jsonObject.toString();
        System.out.println("=========================请求MQ参数值为=========================");
        System.out.println(toString);
        rabbitSenderService.sendTasks(toString);
        return i;
    }

    /**
     * 修改存储任务信息的
     *
     * @param wpTasks 存储任务信息的
     * @return 结果
     */
    @Override
    public int updateWpTasks(WpTasks wpTasks)
    {
        wpTasks.setUpdateTime(DateUtils.getNowDate());
        return wpTasksMapper.updateWpTasks(wpTasks);
    }

    /**
     * 批量删除存储任务信息的
     *
     * @param ids 需要删除的存储任务信息的主键
     * @return 结果
     */
    @Override
    public int deleteWpTasksByIds(Integer[] ids)
    {
        return wpTasksMapper.deleteWpTasksByIds(ids);
    }

    /**
     * 删除存储任务信息的信息
     *
     * @param id 存储任务信息的主键
     * @return 结果
     */
    @Override
    public int deleteWpTasksById(Integer id)
    {
        return wpTasksMapper.deleteWpTasksById(id);
    }

    @Override
    public int tasksArticlesAdd(WpTasks wpTasks) {
        if(wpTasks.getTasksType()==null){
            throw new ServiceException("任务类型参数不能为空！");
        }
        if(wpTasks.getRepetitionsPerRequest()==null || wpTasks.getRepetitionsPerRequest()==0){
            throw new ServiceException("请求次数不能为空！");
        }
        wpTasks.setCreateTime(DateUtils.getNowDate());
        wpTasks.setUserId(SecurityUtils.getUserId());
        wpTasks.setStatus(WpTasksStatus.TO_EXECUTE.getCode());
        if(!WPTasksType.ADD_ARTICLE.getCode().equals(wpTasks.getTasksType())){
            throw new ServiceException("tasksType参数错误！请检查参数是否正确！");
        }
        if(wpTasks.getTasksOption()==null||wpTasks.getTasksOption()<0){
            throw new ServiceException("添加文章，参数值tasksOption不能为空！");
        }
        int result = wpTasksMapper.insertWpTasks(wpTasks);

        //查询分组
        Long openaiMsgGroupsId= wpTasks.getOpenaiMsgGroupsId();
        OpenaiMsg openaiMsg = new OpenaiMsg();
        openaiMsg.setOpenaiMsgGroupsId(openaiMsgGroupsId);
        List<OpenaiMsg> msgList = iOpenaiMsgService.selectOpenaiMsgList(openaiMsg);

        if(msgList.size()==0){
            throw new ServiceException("msgList信息未查到！请检查分组是否正确！");
        }

        //查询标题列
        DictKeywordTitle dictTitle = new DictKeywordTitle();
        dictTitle.setCategoryParentId(wpTasks.getTasksOption());
        List<DictKeywordTitle> dictKeywordTitleList = iDictKeywordTitleService.selectDictKeywordTitleList(dictTitle);

        if(dictKeywordTitleList.size()==0){
            throw  new ServiceException("标题数据查询为空！请先给对应关键词生成标题！");
        }

        System.out.println("数据库参数添加成功，添加文章发送MQ执行中...");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("wpTasks",wpTasks);
        jsonObject.put("msgList",msgList);
        Integer repetitionsPerRequest = wpTasks.getRepetitionsPerRequest();
        for (int i = 0 ; i < repetitionsPerRequest; i ++){

            //获取需要生成的关键词标题
            int dictIndex = i % dictKeywordTitleList.size();
            DictKeywordTitle dictKeywordTitle = dictKeywordTitleList.get(dictIndex);

            //发送第一个MQ请求之前，更新状态
            if(i==0){
                WpTasks updateTasks = new WpTasks();
                updateTasks.setId(wpTasks.getId());
                updateTasks.setStatus(WpTasksStatus.IN_EXECUTE.getCode());
                this.updateWpTasks(updateTasks);
            }
            //组装生成的请求标题参数
            jsonObject.put("dictKeywordTitle",dictKeywordTitle);
            System.out.println("=========================请求MQ参数值为=========================");
            String json = jsonObject.toString();
            System.out.println(json);
            rabbitSenderService.sendCreateArticles(json);
        }

        return result;
    }
}
