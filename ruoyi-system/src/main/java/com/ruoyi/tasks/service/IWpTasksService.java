package com.ruoyi.tasks.service;

import com.ruoyi.tasks.domain.WpTasks;

import java.util.List;

/**
 * 存储任务信息的Service接口
 *
 * @author xialang
 * @date 2023-06-01
 */
public interface IWpTasksService
{
    /**
     * 查询存储任务信息的
     *
     * @param id 存储任务信息的主键
     * @return 存储任务信息的
     */
    public WpTasks selectWpTasksById(Integer id);

    /**
     * 查询存储任务信息的列表
     *
     * @param wpTasks 存储任务信息的
     * @return 存储任务信息的集合
     */
    public List<WpTasks> selectWpTasksList(WpTasks wpTasks);

    /**
     * 新增存储任务信息的
     *
     * @param wpTasks 存储任务信息的
     * @return 结果
     */
    public int insertWpTasks(WpTasks wpTasks);

    /**
     * 修改存储任务信息的
     *
     * @param wpTasks 存储任务信息的
     * @return 结果
     */
    public int updateWpTasks(WpTasks wpTasks);

    /**
     * 批量删除存储任务信息的
     *
     * @param ids 需要删除的存储任务信息的主键集合
     * @return 结果
     */
    public int deleteWpTasksByIds(Integer[] ids);

    /**
     * 删除存储任务信息的信息
     *
     * @param id 存储任务信息的主键
     * @return 结果
     */
    public int deleteWpTasksById(Integer id);

    int tasksArticlesAdd(WpTasks wpTasks);
}
