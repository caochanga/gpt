package com.ruoyi.rabbit.service;

public interface ManageRabbitService {
    void wpTasks(String json);

    void manageDictTitle(String json);

    void createArticles(String json);

}
