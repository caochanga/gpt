package com.ruoyi.rabbit.service.impl;

import com.ruoyi.rabbit.service.RabbitSenderService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("rabbitSenderService")
public class RabbitSenderServiceImpl implements RabbitSenderService {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    //定义消息声明
    final RabbitTemplate.ConfirmCallback confirmCallback = new RabbitTemplate.ConfirmCallback() {
        @Override
        public void confirm(CorrelationData correlationData, boolean ack, String cause) {
            if (ack) {
                System.out.println("MQ发送成功!");
            }
        }
    };


    //定义消息返回
    final RabbitTemplate.ReturnCallback returnCallback = new RabbitTemplate.ReturnCallback() {
        @Override
        public void returnedMessage(org.springframework.amqp.core.Message message, int replyCode,
                                    String replyTest, String exchange, String routingKey) {
            System.err.println("return: " + exchange + ",routingKey:" + routingKey + ",replyCode: " + replyCode + ",replyTest:" + replyTest);
            System.out.println("消息：,发送失败!");
            System.out.println(message);
        }
    };

    @Override
    public void sendTasks(String json) {

        System.out.println("sendTasksMQ消息发送数据为============》");
        System.out.println(json);
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("wpTasks", "wpTasks.callback", json, correlationData);
    }

    @Override
    public void sendDictTitle(String json) {
        System.out.println("sendDictTitleMQ消息发送数据为============》");
        System.out.println(json);
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("DictTitle", "DictTitle.callback", json, correlationData);
    }

    @Override
    public void sendCreateArticles(String json) {
        System.out.println("sendCreateArticles消息发送数据为============》");
        System.out.println(json);
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("sendCreateArticles", "sendCreateArticles.callback", json, correlationData);

    }

    @Override
    public void sendTimeJobArticles(String json) {
        System.out.println("sendTimeJobArticles消息发送数据为============》");
        System.out.println(json);
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("sendTimeJobArticles", "sendTimeJobArticles.callback", json, correlationData);
    }

}
