package com.ruoyi.rabbit.service;

public interface RabbitSenderService {

    void sendTasks(String json);

    void sendDictTitle(String toString);

    void sendCreateArticles(String toString);


    void sendTimeJobArticles(String toString);
}
