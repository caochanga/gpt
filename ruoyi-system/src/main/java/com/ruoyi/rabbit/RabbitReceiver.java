package com.ruoyi.rabbit;

import com.rabbitmq.client.Channel;
import com.ruoyi.rabbit.service.ManageRabbitService;
import com.ruoyi.rabbit.service.RabbitTimeJob;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class RabbitReceiver {

    @Autowired
    ManageRabbitService manageRabbitService;
    @Autowired
    RabbitTimeJob rabbitTimeJob;

    /**
     * 请求创建文章标题MQ队列
     * @param json 请求来的参数
     * @param channel
     * @param headers
     * @throws IOException
     */
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${spring.rabbitmq.listener.wpTasks.queue.name}",
            durable = "${spring.rabbitmq.listener.wpTasks.queue.durable}"),
            exchange = @Exchange(value = "${spring.rabbitmq.listener.wpTasks.exchange.name}",
                    durable = "${spring.rabbitmq.listener.wpTasks.exchange.durable}",
                    type = "${spring.rabbitmq.listener.wpTasks.exchange.type}"),
            ignoreDeclarationExceptions = "${spring.rabbitmq.listener.wpTasks.exchange.ignoreDeclarationExceptions}",
            key = "${spring.rabbitmq.listener.wpTasks.key}"))
    @RabbitHandler
    public void wpTasks(@Payload String json, Channel channel,@Headers Map<String, Object> headers) throws IOException {
        try{

            System.out.println("=====================MQ---wpTasks---接收到参数=========================》");
            System.out.println(json);
            manageRabbitService.wpTasks(json);

        }catch (Exception e){
            System.out.println("数据插入数据库失败！错误信息是=");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }finally {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            //手工ACK
            channel.basicAck(deliveryTag, false);
        }
    }

    /**
     * 创建文章标题MQ,执行次数
     * @param json 请求来的参数
     * @param channel
     * @param headers
     * @throws IOException
     */
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${spring.rabbitmq.listener.DictTitle.queue.name}",
            durable = "${spring.rabbitmq.listener.DictTitle.queue.durable}"),
            exchange = @Exchange(value = "${spring.rabbitmq.listener.DictTitle.exchange.name}",
                    durable = "${spring.rabbitmq.listener.DictTitle.exchange.durable}",
                    type = "${spring.rabbitmq.listener.DictTitle.exchange.type}"),
            ignoreDeclarationExceptions = "${spring.rabbitmq.listener.DictTitle.exchange.ignoreDeclarationExceptions}",
            key = "${spring.rabbitmq.listener.DictTitle.key}"))
    @RabbitHandler
    public void sendDictTitle(@Payload String json, Channel channel,@Headers  Map<String, Object> headers) throws IOException {
        try{

            System.out.println("=====================MQ---DictTitle---接收到参数=========================》");
            System.out.println(json);
            manageRabbitService.manageDictTitle(json);

        }catch (Exception e){
            System.out.println("数据插入数据库失败！错误信息是=");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }finally {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            //手工ACK
            channel.basicAck(deliveryTag, false);
        }
    }

    /**
     * 创建文章MQ,执行次数
     * @param json 请求来的参数
     * @param channel
     * @param headers
     * @throws IOException
     */
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${spring.rabbitmq.listener.sendCreateArticles.queue.name}",
            durable = "${spring.rabbitmq.listener.sendCreateArticles.queue.durable}"),
            exchange = @Exchange(value = "${spring.rabbitmq.listener.sendCreateArticles.exchange.name}",
                    durable = "${spring.rabbitmq.listener.sendCreateArticles.exchange.durable}",
                    type = "${spring.rabbitmq.listener.sendCreateArticles.exchange.type}"),
            ignoreDeclarationExceptions = "${spring.rabbitmq.listener.sendCreateArticles.exchange.ignoreDeclarationExceptions}",
            key = "${spring.rabbitmq.listener.sendCreateArticles.key}"))
    @RabbitHandler
    public void createArticles(@Payload String json, Channel channel,@Headers  Map<String, Object> headers) throws IOException {
        try{

            System.out.println("=====================MQ---sendCreateArticles---接收到参数=========================》");
            System.out.println(json);
            manageRabbitService.createArticles(json);

        }catch (Exception e){
            System.out.println("数据插入数据库失败！错误信息是=");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }finally {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            //手工ACK
            channel.basicAck(deliveryTag, false);
        }
    }

    /**
     * 创建文章MQ,执行次数
     * @param json 请求来的参数
     * @param channel
     * @param headers
     * @throws IOException
     */
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${spring.rabbitmq.listener.sendTimeJobArticles.queue.name}",
            durable = "${spring.rabbitmq.listener.sendTimeJobArticles.queue.durable}"),
            exchange = @Exchange(value = "${spring.rabbitmq.listener.sendTimeJobArticles.exchange.name}",
                    durable = "${spring.rabbitmq.listener.sendTimeJobArticles.exchange.durable}",
                    type = "${spring.rabbitmq.listener.sendTimeJobArticles.exchange.type}"),
            ignoreDeclarationExceptions = "${spring.rabbitmq.listener.sendTimeJobArticles.exchange.ignoreDeclarationExceptions}",
            key = "${spring.rabbitmq.listener.sendTimeJobArticles.key}"))
    @RabbitHandler
    public void timeJobArticles(@Payload String json, Channel channel,@Headers  Map<String, Object> headers) throws IOException {
        try{

            System.out.println("=====================MQ---sendTimeJobArticles---接收到参数=========================》");
            System.out.println(json);
            rabbitTimeJob.timeJobArticles(json);

        }catch (Exception e){
            System.out.println("数据插入数据库失败！错误信息是=");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }finally {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            //手工ACK
            channel.basicAck(deliveryTag, false);
        }
    }

}
