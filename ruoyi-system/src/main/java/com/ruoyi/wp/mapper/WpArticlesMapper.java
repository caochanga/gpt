package com.ruoyi.wp.mapper;

import com.ruoyi.wp.domain.WpArticles;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文章Mapper接口
 * 
 * @author xialang
 * @date 2023-06-05
 */
public interface WpArticlesMapper 
{
    /**
     * 查询文章
     * 
     * @param articlesId 文章主键
     * @return 文章
     */
    public WpArticles selectWpArticlesByArticlesId(Long articlesId);

    /**
     * 查询文章列表
     * 
     * @param wpArticles 文章
     * @return 文章集合
     */
    public List<WpArticles> selectWpArticlesList(WpArticles wpArticles);

    /**
     * 新增文章
     * 
     * @param wpArticles 文章
     * @return 结果
     */
    public int insertWpArticles(WpArticles wpArticles);

    /**
     * 修改文章
     * 
     * @param wpArticles 文章
     * @return 结果
     */
    public int updateWpArticles(WpArticles wpArticles);

    /**
     * 删除文章
     * 
     * @param articlesId 文章主键
     * @return 结果
     */
    public int deleteWpArticlesByArticlesId(Long articlesId);

    /**
     * 批量删除文章
     * 
     * @param articlesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWpArticlesByArticlesIds(Long[] articlesIds);

    List<WpArticles> selectByStatus(@Param("wpArticles") WpArticles wpArticles, @Param("wpKeyIds") Long[] wpKeyIds);
}
