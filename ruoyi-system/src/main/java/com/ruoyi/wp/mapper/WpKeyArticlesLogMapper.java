package com.ruoyi.wp.mapper;

import com.ruoyi.wp.domain.WpKeyArticlesLog;

import java.util.List;

public interface WpKeyArticlesLogMapper {
    /**
     * 查询PHP文章发布表
     *
     * @param id PHP文章发布表主键
     * @return PHP文章发布表
     */
    public WpKeyArticlesLog selectWpKeyArticlesLogById(Long id);

    /**
     * 查询PHP文章发布表列表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return PHP文章发布表集合
     */
    public List<WpKeyArticlesLog> selectWpKeyArticlesLogList(WpKeyArticlesLog wpKeyArticlesLog);

    /**
     * 新增PHP文章发布表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return 结果
     */
    public int insertWpKeyArticlesLog(WpKeyArticlesLog wpKeyArticlesLog);

    /**
     * 修改PHP文章发布表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return 结果
     */
    public int updateWpKeyArticlesLog(WpKeyArticlesLog wpKeyArticlesLog);

    /**
     * 删除PHP文章发布表
     *
     * @param id PHP文章发布表主键
     * @return 结果
     */
    public int deleteWpKeyArticlesLogById(Long id);

    /**
     * 批量删除PHP文章发布表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWpKeyArticlesLogByIds(Long[] ids);
}
