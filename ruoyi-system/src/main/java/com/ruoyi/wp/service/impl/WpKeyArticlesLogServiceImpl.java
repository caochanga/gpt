package com.ruoyi.wp.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wp.domain.WpKeyArticlesLog;
import com.ruoyi.wp.mapper.WpKeyArticlesLogMapper;
import com.ruoyi.wp.service.IWpKeyArticlesLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WpKeyArticlesLogServiceImpl implements IWpKeyArticlesLogService {
    @Autowired
    private WpKeyArticlesLogMapper wpKeyArticlesLogMapper;

    /**
     * 查询PHP文章发布表
     *
     * @param id PHP文章发布表主键
     * @return PHP文章发布表
     */
    @Override
    public WpKeyArticlesLog selectWpKeyArticlesLogById(Long id)
    {
        return wpKeyArticlesLogMapper.selectWpKeyArticlesLogById(id);
    }

    /**
     * 查询PHP文章发布表列表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return PHP文章发布表
     */
    @Override
    public List<WpKeyArticlesLog> selectWpKeyArticlesLogList(WpKeyArticlesLog wpKeyArticlesLog)
    {
        return wpKeyArticlesLogMapper.selectWpKeyArticlesLogList(wpKeyArticlesLog);
    }

    /**
     * 新增PHP文章发布表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return 结果
     */
    @Override
    public int insertWpKeyArticlesLog(WpKeyArticlesLog wpKeyArticlesLog)
    {
        wpKeyArticlesLog.setCreateTime(DateUtils.getNowDate());
        return wpKeyArticlesLogMapper.insertWpKeyArticlesLog(wpKeyArticlesLog);
    }

    /**
     * 修改PHP文章发布表
     *
     * @param wpKeyArticlesLog PHP文章发布表
     * @return 结果
     */
    @Override
    public int updateWpKeyArticlesLog(WpKeyArticlesLog wpKeyArticlesLog)
    {
        wpKeyArticlesLog.setUpdateTime(DateUtils.getNowDate());
        return wpKeyArticlesLogMapper.updateWpKeyArticlesLog(wpKeyArticlesLog);
    }

    /**
     * 批量删除PHP文章发布表
     *
     * @param ids 需要删除的PHP文章发布表主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyArticlesLogByIds(Long[] ids)
    {
        return wpKeyArticlesLogMapper.deleteWpKeyArticlesLogByIds(ids);
    }

    /**
     * 删除PHP文章发布表信息
     *
     * @param id PHP文章发布表主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyArticlesLogById(Long id)
    {
        return wpKeyArticlesLogMapper.deleteWpKeyArticlesLogById(id);
    }
}
