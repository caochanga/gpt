package com.ruoyi.wp.service;

import com.ruoyi.wp.domain.WpKeyCategory;
import com.ruoyi.wp.domain.dto.WpKeyCategoryListDto;

import java.util.List;

/**
 * wordpress文章分类Service接口
 *
 * @author ruoyi
 * @date 2023-06-07
 */
public interface IWpKeyCategoryService
{
    /**
     * 查询wordpress文章分类
     *
     * @param id wordpress文章分类主键
     * @return wordpress文章分类
     */
    public WpKeyCategory selectWpKeyCategoryById(Long id);

    /**
     * 查询wordpress文章分类列表
     *
     * @param wpKeyCategory wordpress文章分类
     * @return wordpress文章分类集合
     */
    public List<WpKeyCategory> selectWpKeyCategoryList(WpKeyCategory wpKeyCategory);

    /**
     * 新增wordpress文章分类
     *
     * @param wpKeyCategory wordpress文章分类
     * @return 结果
     */
    public int insertWpKeyCategory(WpKeyCategory wpKeyCategory);

    /**
     * 修改wordpress文章分类
     *
     * @param wpKeyCategory wordpress文章分类
     * @return 结果
     */
    public int updateWpKeyCategory(WpKeyCategory wpKeyCategory);

    /**
     * 批量删除wordpress文章分类
     *
     * @param ids 需要删除的wordpress文章分类主键集合
     * @return 结果
     */
    public int deleteWpKeyCategoryByIds(Long[] ids);

    /**
     * 删除wordpress文章分类信息
     *
     * @param id wordpress文章分类主键
     * @return 结果
     */
    public int deleteWpKeyCategoryById(Long id);

    int addList( WpKeyCategoryListDto dto );
}
