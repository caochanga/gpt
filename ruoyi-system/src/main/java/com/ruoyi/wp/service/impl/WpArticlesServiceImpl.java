package com.ruoyi.wp.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wp.domain.WpArticles;
import com.ruoyi.wp.mapper.WpArticlesMapper;
import com.ruoyi.wp.service.IWpArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章Service业务层处理
 * 
 * @author xialang
 * @date 2023-06-05
 */
@Service
public class WpArticlesServiceImpl implements IWpArticlesService 
{
    @Autowired
    private WpArticlesMapper wpArticlesMapper;

    /**
     * 查询文章
     * 
     * @param articlesId 文章主键
     * @return 文章
     */
    @Override
    public WpArticles selectWpArticlesByArticlesId(Long articlesId)
    {
        return wpArticlesMapper.selectWpArticlesByArticlesId(articlesId);
    }

    /**
     * 查询文章列表
     * 
     * @param wpArticles 文章
     * @return 文章
     */
    @Override
    public List<WpArticles> selectWpArticlesList(WpArticles wpArticles)
    {
        return wpArticlesMapper.selectWpArticlesList(wpArticles);
    }

    /**
     * 新增文章
     * 
     * @param wpArticles 文章
     * @return 结果
     */
    @Override
    public int insertWpArticles(WpArticles wpArticles)
    {
        wpArticles.setCreateTime(DateUtils.getNowDate());
        return wpArticlesMapper.insertWpArticles(wpArticles);
    }

    /**
     * 修改文章
     * 
     * @param wpArticles 文章
     * @return 结果
     */
    @Override
    public int updateWpArticles(WpArticles wpArticles)
    {
        wpArticles.setUpdateTime(DateUtils.getNowDate());
        return wpArticlesMapper.updateWpArticles(wpArticles);
    }

    /**
     * 批量删除文章
     * 
     * @param articlesIds 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteWpArticlesByArticlesIds(Long[] articlesIds)
    {
        return wpArticlesMapper.deleteWpArticlesByArticlesIds(articlesIds);
    }

    /**
     * 删除文章信息
     * 
     * @param articlesId 文章主键
     * @return 结果
     */
    @Override
    public int deleteWpArticlesByArticlesId(Long articlesId)
    {
        return wpArticlesMapper.deleteWpArticlesByArticlesId(articlesId);
    }

    @Override
    public List<WpArticles> selectByStatus(WpArticles wpArticles,Long[] wpKeyIds) {
        return wpArticlesMapper.selectByStatus(wpArticles,wpKeyIds);
    }
}
