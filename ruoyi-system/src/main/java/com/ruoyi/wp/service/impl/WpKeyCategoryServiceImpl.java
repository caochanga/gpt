package com.ruoyi.wp.service.impl;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.wp.domain.WpKeyCategory;
import com.ruoyi.wp.domain.dto.WpKeyCategoryListDto;
import com.ruoyi.wp.mapper.WpKeyCategoryMapper;
import com.ruoyi.wp.service.IWpKeyCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * wordpress文章分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-06-07
 */
@Service
public class WpKeyCategoryServiceImpl implements IWpKeyCategoryService
{
    @Autowired
    private WpKeyCategoryMapper wpKeyCategoryMapper;

    /**
     * 查询wordpress文章分类
     *
     * @param id wordpress文章分类主键
     * @return wordpress文章分类
     */
    @Override
    public WpKeyCategory selectWpKeyCategoryById(Long id)
    {
        return wpKeyCategoryMapper.selectWpKeyCategoryById(id);
    }

    /**
     * 查询wordpress文章分类列表
     *
     * @param wpKeyCategory wordpress文章分类
     * @return wordpress文章分类
     */
    @Override
    public List<WpKeyCategory> selectWpKeyCategoryList(WpKeyCategory wpKeyCategory)
    {
        return wpKeyCategoryMapper.selectWpKeyCategoryList(wpKeyCategory);
    }

    /**
     * 新增wordpress文章分类
     *
     * @param wpKeyCategory wordpress文章分类
     * @return 结果
     */
    @Override
    public int insertWpKeyCategory(WpKeyCategory wpKeyCategory)
    {
        wpKeyCategory.setCreateTime(DateUtils.getNowDate());
        return wpKeyCategoryMapper.insertWpKeyCategory(wpKeyCategory);
    }

    /**
     * 修改wordpress文章分类
     *
     * @param wpKeyCategory wordpress文章分类
     * @return 结果
     */
    @Override
    public int updateWpKeyCategory(WpKeyCategory wpKeyCategory)
    {
        wpKeyCategory.setUpdateTime(DateUtils.getNowDate());
        return wpKeyCategoryMapper.updateWpKeyCategory(wpKeyCategory);
    }

    /**
     * 批量删除wordpress文章分类
     *
     * @param ids 需要删除的wordpress文章分类主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyCategoryByIds(Long[] ids)
    {
        return wpKeyCategoryMapper.deleteWpKeyCategoryByIds(ids);
    }

    /**
     * 删除wordpress文章分类信息
     *
     * @param id wordpress文章分类主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyCategoryById(Long id)
    {
        return wpKeyCategoryMapper.deleteWpKeyCategoryById(id);
    }

    @Override
    public int addList( WpKeyCategoryListDto dto) {
        //参数赋值
        List<WpKeyCategory> wpKeyCategoryList = dto.getWpKeyCategoryList();
        Long wpKeyId  = dto.getWpKeyId();
        Long userId = SecurityUtils.getUserId();

        //查询默认的
        WpKeyCategory wpKeyCategoryQuery = new WpKeyCategory();
        wpKeyCategoryQuery.setWpKeyId(wpKeyId);
        //如果提交上来的值是0，你么则代表清空
        if(wpKeyCategoryList.size()==0){
            wpKeyCategoryMapper.removeByCondition(wpKeyCategoryQuery);
            return 1;
        }
        //删除原有的全部，然后重新再添加一遍
        //查询原有的键值对
        List<WpKeyCategory> wpKeyCategories = wpKeyCategoryMapper.selectWpKeyCategoryList(wpKeyCategoryQuery);
        if(wpKeyCategories.size()>0){
            Long[] wpKeyIds = wpKeyCategories.stream().map(WpKeyCategory::getId).toArray(Long[]::new);
            wpKeyCategoryMapper.deleteWpKeyCategoryByIds(wpKeyIds);
        }
        for (int i = 0 ; i<wpKeyCategoryList.size(); i++){
            WpKeyCategory old = wpKeyCategoryList.get(i);
            Long dictCategoryId = old.getDictCategoryId();
            Long wpCategory = old.getWpCategory();

            WpKeyCategory wpKeyCategory = new WpKeyCategory();
            wpKeyCategory.setUserId(userId);
            wpKeyCategory.setDictCategoryId(dictCategoryId);
            wpKeyCategory.setWpCategory(wpCategory);
            wpKeyCategory.setWpKeyId(wpKeyId);

            if(dictCategoryId==null || wpCategory==null){
                throw new ServiceException("请求参数异常！非法空值！");
            }
            wpKeyCategoryMapper.insertWpKeyCategory(wpKeyCategory);
        }
        return 1;
    }


}
