package com.ruoyi.wp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章对象 wp_articles
 * 
 * @author xialang
 * @date 2023-06-05
 */
public class WpArticles extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章ID */
    private Long articlesId;

    /** 网站api信息Id */
    private Long wpKeyId;

    /** 用户id信息 */
    @Excel(name = "用户id信息")
    private Long userId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String title;

    /** 关键词（逗号分隔的字符串） */
    @Excel(name = "关键词", readConverterExp = "逗=号分隔的字符串")
    private String keywords;

    /** 主内容 */
    @Excel(name = "主内容")
    private String content;

    /** 摘要（可选） */
    @Excel(name = "摘要", readConverterExp = "可=选")
    private String summary;

    /** 文章状态（1-草稿、2-已发布、3-已归档） */
    @Excel(name = "文章状态", readConverterExp = "1=-草稿、2-已发布、3-已归档")
    private Integer status;

    /** 文章分类ID */
    @Excel(name = "文章分类ID")
    private Long categoryId;

    public void setArticlesId(Long articlesId) 
    {
        this.articlesId = articlesId;
    }

    public Long getArticlesId() 
    {
        return articlesId;
    }
    public void setWpKeyId(Long wpKeyId) 
    {
        this.wpKeyId = wpKeyId;
    }

    public Long getWpKeyId() 
    {
        return wpKeyId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public String convertContentToHtml() {

        return content.replace("\n", "<br>");
    }
    public void setSummary(String summary) 
    {
        this.summary = summary;
    }

    public String getSummary() 
    {
        return summary;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("articlesId", getArticlesId())
            .append("wpKeyId", getWpKeyId())
            .append("userId", getUserId())
            .append("title", getTitle())
            .append("keywords", getKeywords())
            .append("content", getContent())
            .append("summary", getSummary())
            .append("status", getStatus())
            .append("categoryId", getCategoryId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
