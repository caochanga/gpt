package com.ruoyi.wp.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class WpKeyArticlesLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long wpKeyId;

    /** 文章Id */
    @Excel(name = "文章Id")
    private Long wpArticlesId;

    /** 状态值 1、已发布 2、未发布 */
    @Excel(name = "状态值 1、已发布 2、未发布")
    private Integer status;

    /** 发布到了那个分类 */
    @Excel(name = "发布到了那个分类")
    private Long wpKeyCategotyId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long userId;

    /** 请求PHP返回ID信息 */
    @Excel(name = "请求PHP返回ID信息")
    private Long phpArticlesId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setWpKeyId(Long wpKeyId)
    {
        this.wpKeyId = wpKeyId;
    }

    public Long getWpKeyId()
    {
        return wpKeyId;
    }
    public void setWpArticlesId(Long wpArticlesId)
    {
        this.wpArticlesId = wpArticlesId;
    }

    public Long getWpArticlesId()
    {
        return wpArticlesId;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setWpKeyCategotyId(Long wpKeyCategotyId)
    {
        this.wpKeyCategotyId = wpKeyCategotyId;
    }

    public Long getWpKeyCategotyId()
    {
        return wpKeyCategotyId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setPhpArticlesId(Long phpArticlesId)
    {
        this.phpArticlesId = phpArticlesId;
    }

    public Long getPhpArticlesId()
    {
        return phpArticlesId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("wpKeyId", getWpKeyId())
                .append("wpArticlesId", getWpArticlesId())
                .append("status", getStatus())
                .append("wpKeyCategotyId", getWpKeyCategotyId())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("userId", getUserId())
                .append("phpArticlesId", getPhpArticlesId())
                .toString();
    }
}