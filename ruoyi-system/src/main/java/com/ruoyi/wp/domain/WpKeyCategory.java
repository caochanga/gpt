package com.ruoyi.wp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * wordpress文章分类对象 wp_key_category
 *
 * @author ruoyi
 * @date 2023-06-07
 */
public class WpKeyCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** wp_key_id主键 */
    @Excel(name = "wp_key_id主键")
    private Long wpKeyId;

    /** wordpres文章分类 */
    @Excel(name = "wordpres文章分类")
    private Long wpCategory;

    /** 创建人用户ID信息 */
    @Excel(name = "创建人用户ID信息")
    private Long userId;

    /** 数据库字典表Id */
    @Excel(name = "数据库字典表Id")
    private Long dictCategoryId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setWpKeyId(Long wpKeyId)
    {
        this.wpKeyId = wpKeyId;
    }

    public Long getWpKeyId()
    {
        return wpKeyId;
    }
    public void setWpCategory(Long wpCategory)
    {
        this.wpCategory = wpCategory;
    }

    public Long getWpCategory()
    {
        return wpCategory;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setDictCategoryId(Long dictCategoryId)
    {
        this.dictCategoryId = dictCategoryId;
    }

    public Long getDictCategoryId()
    {
        return dictCategoryId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("wpKeyId", getWpKeyId())
                .append("wpCategory", getWpCategory())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("userId", getUserId())
                .append("dictCategoryId", getDictCategoryId())
                .toString();
    }
}
