package com.ruoyi.wp.domain.dto;

import com.ruoyi.wp.domain.WpKeyCategory;

import java.util.List;

public class WpKeyCategoryListDto {
    private List<WpKeyCategory> wpKeyCategoryList;
    private Long wpKeyId;

    public List<WpKeyCategory> getWpKeyCategoryList() {
        return wpKeyCategoryList;
    }

    public void setWpKeyCategoryList(List<WpKeyCategory> wpKeyCategoryList) {
        this.wpKeyCategoryList = wpKeyCategoryList;
    }

    public Long getWpKeyId() {
        return wpKeyId;
    }

    public void setWpKeyId(Long wpKeyId) {
        this.wpKeyId = wpKeyId;
    }
}
