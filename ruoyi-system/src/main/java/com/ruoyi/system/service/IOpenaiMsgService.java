package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OpenaiMsg;

/**
 * 存储对话消息的Service接口
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
public interface IOpenaiMsgService 
{
    /**
     * 查询存储对话消息的
     * 
     * @param id 存储对话消息的主键
     * @return 存储对话消息的
     */
    public OpenaiMsg selectOpenaiMsgById(Long id);

    /**
     * 查询存储对话消息的列表
     * 
     * @param openaiMsg 存储对话消息的
     * @return 存储对话消息的集合
     */
    public List<OpenaiMsg> selectOpenaiMsgList(OpenaiMsg openaiMsg);

    /**
     * 新增存储对话消息的
     * 
     * @param openaiMsg 存储对话消息的
     * @return 结果
     */
    public int insertOpenaiMsg(OpenaiMsg openaiMsg);

    /**
     * 修改存储对话消息的
     * 
     * @param openaiMsg 存储对话消息的
     * @return 结果
     */
    public int updateOpenaiMsg(OpenaiMsg openaiMsg);

    /**
     * 批量删除存储对话消息的
     * 
     * @param ids 需要删除的存储对话消息的主键集合
     * @return 结果
     */
    public int deleteOpenaiMsgByIds(Long[] ids);

    /**
     * 删除存储对话消息的信息
     * 
     * @param id 存储对话消息的主键
     * @return 结果
     */
    public int deleteOpenaiMsgById(Long id);
}
