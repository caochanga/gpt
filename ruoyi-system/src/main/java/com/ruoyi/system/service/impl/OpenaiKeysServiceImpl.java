package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.OpenaiKeys;
import com.ruoyi.system.mapper.OpenaiKeysMapper;
import com.ruoyi.system.service.IOpenaiKeysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * OpenAI密钥存储Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
@Service
public class OpenaiKeysServiceImpl implements IOpenaiKeysService 
{
    @Autowired
    private OpenaiKeysMapper openaiKeysMapper;

    /**
     * 查询OpenAI密钥存储
     * 
     * @param id OpenAI密钥存储主键
     * @return OpenAI密钥存储
     */
    @Override
    public OpenaiKeys selectOpenaiKeysById(Long id)
    {
        return openaiKeysMapper.selectOpenaiKeysById(id);
    }

    /**
     * 查询OpenAI密钥存储列表
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return OpenAI密钥存储
     */
    @Override
    public List<OpenaiKeys> selectOpenaiKeysList(OpenaiKeys openaiKeys)
    {
        return openaiKeysMapper.selectOpenaiKeysList(openaiKeys);
    }

    /**
     * 新增OpenAI密钥存储
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return 结果
     */
    @Override
    public int insertOpenaiKeys(OpenaiKeys openaiKeys)
    {
        openaiKeys.setCreateTime(DateUtils.getNowDate());
        return openaiKeysMapper.insertOpenaiKeys(openaiKeys);
    }

    /**
     * 修改OpenAI密钥存储
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return 结果
     */
    @Override
    public int updateOpenaiKeys(OpenaiKeys openaiKeys)
    {
        openaiKeys.setUpdateTime(DateUtils.getNowDate());
        return openaiKeysMapper.updateOpenaiKeys(openaiKeys);
    }

    /**
     * 批量删除OpenAI密钥存储
     * 
     * @param ids 需要删除的OpenAI密钥存储主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiKeysByIds(Long[] ids)
    {
        return openaiKeysMapper.deleteOpenaiKeysByIds(ids);
    }

    /**
     * 删除OpenAI密钥存储信息
     * 
     * @param id OpenAI密钥存储主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiKeysById(Long id)
    {
        return openaiKeysMapper.deleteOpenaiKeysById(id);
    }

    @Override
    public List<String> getApiList() {
        return openaiKeysMapper.getApiList();
    }
}
