package com.ruoyi.system.service;

import com.ruoyi.system.domain.DictKeywordTitle;

import java.util.List;

/**
 * 关键词标题生成Service接口
 * 
 * @author xialang
 * @date 2023-05-30
 */
public interface IDictKeywordTitleService 
{
    /**
     * 查询关键词标题生成
     * 
     * @param id 关键词标题生成主键
     * @return 关键词标题生成
     */
    public DictKeywordTitle selectDictKeywordTitleById(Long id);

    /**
     * 查询关键词标题生成列表
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 关键词标题生成集合
     */
    public List<DictKeywordTitle> selectDictKeywordTitleList(DictKeywordTitle dictKeywordTitle);
    /**
     * 新增关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    public int insertDictKeywordTitle(DictKeywordTitle dictKeywordTitle);

    /**
     * 修改关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    public int updateDictKeywordTitle(DictKeywordTitle dictKeywordTitle);

    /**
     * 批量删除关键词标题生成
     * 
     * @param ids 需要删除的关键词标题生成主键集合
     * @return 结果
     */
    public int deleteDictKeywordTitleByIds(Long[] ids);

    /**
     * 删除关键词标题生成信息
     * 
     * @param id 关键词标题生成主键
     * @return 结果
     */
    public int deleteDictKeywordTitleById(Long id);
}
