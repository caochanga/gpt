package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.OpenaiUrl;
import com.ruoyi.system.mapper.OpenaiUrlMapper;
import com.ruoyi.system.service.IOpenaiUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * openAI使用地址Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
@Service
public class OpenaiUrlServiceImpl implements IOpenaiUrlService 
{
    @Autowired
    private OpenaiUrlMapper openaiUrlMapper;

    /**
     * 查询openAI使用地址
     * 
     * @param id openAI使用地址主键
     * @return openAI使用地址
     */
    @Override
    public OpenaiUrl selectOpenaiUrlById(Long id)
    {
        return openaiUrlMapper.selectOpenaiUrlById(id);
    }

    /**
     * 查询openAI使用地址列表
     * 
     * @param openaiUrl openAI使用地址
     * @return openAI使用地址
     */
    @Override
    public List<OpenaiUrl> selectOpenaiUrlList(OpenaiUrl openaiUrl)
    {
        return openaiUrlMapper.selectOpenaiUrlList(openaiUrl);
    }

    /**
     * 新增openAI使用地址
     * 
     * @param openaiUrl openAI使用地址
     * @return 结果
     */
    @Override
    public int insertOpenaiUrl(OpenaiUrl openaiUrl)
    {
        openaiUrl.setCreateTime(DateUtils.getNowDate());
        return openaiUrlMapper.insertOpenaiUrl(openaiUrl);
    }

    /**
     * 修改openAI使用地址
     * 
     * @param openaiUrl openAI使用地址
     * @return 结果
     */
    @Override
    public int updateOpenaiUrl(OpenaiUrl openaiUrl)
    {
        openaiUrl.setUpdateTime(DateUtils.getNowDate());
        return openaiUrlMapper.updateOpenaiUrl(openaiUrl);
    }

    /**
     * 批量删除openAI使用地址
     * 
     * @param ids 需要删除的openAI使用地址主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiUrlByIds(Long[] ids)
    {
        return openaiUrlMapper.deleteOpenaiUrlByIds(ids);
    }

    /**
     * 删除openAI使用地址信息
     * 
     * @param id openAI使用地址主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiUrlById(Long id)
    {
        return openaiUrlMapper.deleteOpenaiUrlById(id);
    }
    @Override
    public String getByValidStatus(Integer code) {
        return openaiUrlMapper.getByValidStatus(code);
    }
}
