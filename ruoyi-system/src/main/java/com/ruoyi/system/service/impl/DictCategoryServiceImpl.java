package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.DictCategory;
import com.ruoyi.system.domain.vo.DictCategoryVo;
import com.ruoyi.system.mapper.DictCategoryMapper;
import com.ruoyi.system.service.IDictCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 关键词管理Service业务层处理
 * 
 * @author xialang
 * @date 2023-05-30
 */
@Service
public class DictCategoryServiceImpl implements IDictCategoryService 
{
    @Autowired
    private DictCategoryMapper dictCategoryMapper;

    /**
     * 查询关键词管理
     * 
     * @param id 关键词管理主键
     * @return 关键词管理
     */
    @Override
    public DictCategory selectDictCategoryById(Long id)
    {
        return dictCategoryMapper.selectDictCategoryById(id);
    }

    /**
     * 查询关键词管理列表
     * 
     * @param dictCategory 关键词管理
     * @return 关键词管理
     */
    @Override
    public List<DictCategory> selectDictCategoryList(DictCategory dictCategory)
    {
        return dictCategoryMapper.selectDictCategoryList(dictCategory);
    }
    @Override
    public List<DictCategoryVo> selectDictCategoryListVo(DictCategory dictCategory)
    {
        List<DictCategoryVo> dictCategoryVos = dictCategoryMapper.selectDictCategoryListVo(dictCategory);
        return dictCategoryVos;
    }
    /**
     * 新增关键词管理
     * 
     * @param dictCategory 关键词管理
     * @return 结果
     */
    @Override
    public int insertDictCategory(DictCategory dictCategory)
    {
        dictCategory.setCreateTime(DateUtils.getNowDate());
        dictCategory.setUserId(SecurityUtils.getUserId());

        return dictCategoryMapper.insertDictCategory(dictCategory);
    }

    /**
     * 修改关键词管理
     * 
     * @param dictCategory 关键词管理
     * @return 结果
     */
    @Override
    public int updateDictCategory(DictCategory dictCategory)
    {
        dictCategory.setUpdateTime(DateUtils.getNowDate());
        return dictCategoryMapper.updateDictCategory(dictCategory);
    }

    /**
     * 批量删除关键词管理
     * 
     * @param ids 需要删除的关键词管理主键
     * @return 结果
     */
    @Override
    public int deleteDictCategoryByIds(Long[] ids)
    {
        return dictCategoryMapper.deleteDictCategoryByIds(ids);
    }

    /**
     * 删除关键词管理信息
     * 
     * @param id 关键词管理主键
     * @return 结果
     */
    @Override
    public int deleteDictCategoryById(Long id)
    {
        return dictCategoryMapper.deleteDictCategoryById(id);
    }

    @Override
    public List<DictCategoryVo> listParentIdVo(DictCategory dictCategory) {


        return dictCategoryMapper.listParentIdVo(dictCategory);
    }

    @Override
    public Integer verifyCategory(DictCategory dictCategory) {

        dictCategory.setUserId(SecurityUtils.getUserId());
        return dictCategoryMapper.verifyCategory(dictCategory);
    }
}
