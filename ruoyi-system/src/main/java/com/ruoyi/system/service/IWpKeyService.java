package com.ruoyi.system.service;

import com.ruoyi.system.domain.WpKey;

import java.util.List;

/**
 * 用户网站管理Service接口
 * 
 * @author ruoyi
 * @date 2023-05-26
 */
public interface IWpKeyService 
{
    /**
     * 查询用户网站管理
     * 
     * @param id 用户网站管理主键
     * @return 用户网站管理
     */
    public WpKey selectWpKeyById(Long id);

    /**
     * 查询用户网站管理列表
     * 
     * @param wpKey 用户网站管理
     * @return 用户网站管理集合
     */
    public List<WpKey> selectWpKeyList(WpKey wpKey);

    /**
     * 新增用户网站管理
     * 
     * @param wpKey 用户网站管理
     * @return 结果
     */
    public int insertWpKey(WpKey wpKey);

    /**
     * 修改用户网站管理
     * 
     * @param wpKey 用户网站管理
     * @return 结果
     */
    public int updateWpKey(WpKey wpKey);

    /**
     * 批量删除用户网站管理
     * 
     * @param ids 需要删除的用户网站管理主键集合
     * @return 结果
     */
    public int deleteWpKeyByIds(Long[] ids);

    /**
     * 删除用户网站管理信息
     * 
     * @param id 用户网站管理主键
     * @return 结果
     */
    public int deleteWpKeyById(Long id);
}
