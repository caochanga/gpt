package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.WpKey;
import com.ruoyi.system.mapper.WpKeyMapper;
import com.ruoyi.system.service.IWpKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户网站管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-26
 */
@Service
public class WpKeyServiceImpl implements IWpKeyService
{
    @Autowired
    private WpKeyMapper wpKeyMapper;

    /**
     * 查询用户网站管理
     * 
     * @param id 用户网站管理主键
     * @return 用户网站管理
     */
    @Override
    public WpKey selectWpKeyById(Long id)
    {
        return wpKeyMapper.selectWpKeyById(id);
    }

    /**
     * 查询用户网站管理列表
     * 
     * @param wpKey 用户网站管理
     * @return 用户网站管理
     */
    @Override
    public List<WpKey> selectWpKeyList(WpKey wpKey)
    {
        return wpKeyMapper.selectWpKeyList(wpKey);
    }

    /**
     * 新增用户网站管理
     * 
     * @param wpKey 用户网站管理
     * @return 结果
     */
    @Override
    public int insertWpKey(WpKey wpKey)
    {
        return wpKeyMapper.insertWpKey(wpKey);
    }

    /**
     * 修改用户网站管理
     * 
     * @param wpKey 用户网站管理
     * @return 结果
     */
    @Override
    public int updateWpKey(WpKey wpKey)
    {
        return wpKeyMapper.updateWpKey(wpKey);
    }

    /**
     * 批量删除用户网站管理
     * 
     * @param ids 需要删除的用户网站管理主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyByIds(Long[] ids)
    {
        return wpKeyMapper.deleteWpKeyByIds(ids);
    }

    /**
     * 删除用户网站管理信息
     * 
     * @param id 用户网站管理主键
     * @return 结果
     */
    @Override
    public int deleteWpKeyById(Long id)
    {
        return wpKeyMapper.deleteWpKeyById(id);
    }
}
