package com.ruoyi.system.service;

import com.ruoyi.system.domain.OpenaiUrl;

import java.util.List;

/**
 * openAI使用地址Service接口
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
public interface IOpenaiUrlService 
{
    /**
     * 查询openAI使用地址
     * 
     * @param id openAI使用地址主键
     * @return openAI使用地址
     */
    public OpenaiUrl selectOpenaiUrlById(Long id);

    /**
     * 查询openAI使用地址列表
     * 
     * @param openaiUrl openAI使用地址
     * @return openAI使用地址集合
     */
    public List<OpenaiUrl> selectOpenaiUrlList(OpenaiUrl openaiUrl);

    /**
     * 新增openAI使用地址
     * 
     * @param openaiUrl openAI使用地址
     * @return 结果
     */
    public int insertOpenaiUrl(OpenaiUrl openaiUrl);

    /**
     * 修改openAI使用地址
     * 
     * @param openaiUrl openAI使用地址
     * @return 结果
     */
    public int updateOpenaiUrl(OpenaiUrl openaiUrl);

    /**
     * 批量删除openAI使用地址
     * 
     * @param ids 需要删除的openAI使用地址主键集合
     * @return 结果
     */
    public int deleteOpenaiUrlByIds(Long[] ids);

    /**
     * 删除openAI使用地址信息
     * 
     * @param id openAI使用地址主键
     * @return 结果
     */
    public int deleteOpenaiUrlById(Long id);

    public String getByValidStatus(Integer code);
}
