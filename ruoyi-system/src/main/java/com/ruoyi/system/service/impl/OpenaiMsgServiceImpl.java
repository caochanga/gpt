package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OpenaiMsgMapper;
import com.ruoyi.system.domain.OpenaiMsg;
import com.ruoyi.system.service.IOpenaiMsgService;

/**
 * 存储对话消息的Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
@Service
public class OpenaiMsgServiceImpl implements IOpenaiMsgService 
{
    @Autowired
    private OpenaiMsgMapper openaiMsgMapper;

    /**
     * 查询存储对话消息的
     * 
     * @param id 存储对话消息的主键
     * @return 存储对话消息的
     */
    @Override
    public OpenaiMsg selectOpenaiMsgById(Long id)
    {
        return openaiMsgMapper.selectOpenaiMsgById(id);
    }

    /**
     * 查询存储对话消息的列表
     * 
     * @param openaiMsg 存储对话消息的
     * @return 存储对话消息的
     */
    @Override
    public List<OpenaiMsg> selectOpenaiMsgList(OpenaiMsg openaiMsg)
    {
        return openaiMsgMapper.selectOpenaiMsgList(openaiMsg);
    }

    /**
     * 新增存储对话消息的
     * 
     * @param openaiMsg 存储对话消息的
     * @return 结果
     */
    @Override
    public int insertOpenaiMsg(OpenaiMsg openaiMsg)
    {
        openaiMsg.setCreateTime(DateUtils.getNowDate());
        return openaiMsgMapper.insertOpenaiMsg(openaiMsg);
    }

    /**
     * 修改存储对话消息的
     * 
     * @param openaiMsg 存储对话消息的
     * @return 结果
     */
    @Override
    public int updateOpenaiMsg(OpenaiMsg openaiMsg)
    {
        openaiMsg.setUpdateTime(DateUtils.getNowDate());
        return openaiMsgMapper.updateOpenaiMsg(openaiMsg);
    }

    /**
     * 批量删除存储对话消息的
     * 
     * @param ids 需要删除的存储对话消息的主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiMsgByIds(Long[] ids)
    {
        return openaiMsgMapper.deleteOpenaiMsgByIds(ids);
    }

    /**
     * 删除存储对话消息的信息
     * 
     * @param id 存储对话消息的主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiMsgById(Long id)
    {
        return openaiMsgMapper.deleteOpenaiMsgById(id);
    }
}
