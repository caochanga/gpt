package com.ruoyi.system.service;

import com.ruoyi.system.domain.DictCategory;
import com.ruoyi.system.domain.vo.DictCategoryVo;

import java.util.List;

/**
 * 关键词管理Service接口
 * 
 * @author xialang
 * @date 2023-05-30
 */
public interface IDictCategoryService 
{
    /**
     * 查询关键词管理
     * 
     * @param id 关键词管理主键
     * @return 关键词管理
     */
    public DictCategory selectDictCategoryById(Long id);

    /**
     * 查询关键词管理列表
     * 
     * @param dictCategory 关键词管理
     * @return 关键词管理集合
     */
    public List<DictCategory> selectDictCategoryList(DictCategory dictCategory);
    public List<DictCategoryVo> selectDictCategoryListVo(DictCategory dictCategory);

    /**
     * 新增关键词管理
     * 
     * @param dictCategory 关键词管理
     * @return 结果
     */
    public int insertDictCategory(DictCategory dictCategory);

    /**
     * 修改关键词管理
     * 
     * @param dictCategory 关键词管理
     * @return 结果
     */
    public int updateDictCategory(DictCategory dictCategory);

    /**
     * 批量删除关键词管理
     * 
     * @param ids 需要删除的关键词管理主键集合
     * @return 结果
     */
    public int deleteDictCategoryByIds(Long[] ids);

    /**
     * 删除关键词管理信息
     * 
     * @param id 关键词管理主键
     * @return 结果
     */
    public int deleteDictCategoryById(Long id);

    List<DictCategoryVo> listParentIdVo(DictCategory dictCategory);

    Integer verifyCategory(DictCategory dictCategory);
}
