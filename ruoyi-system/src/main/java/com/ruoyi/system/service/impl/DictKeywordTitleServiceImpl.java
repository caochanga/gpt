package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.DictKeywordTitle;
import com.ruoyi.system.mapper.DictKeywordTitleMapper;
import com.ruoyi.system.service.IDictKeywordTitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 关键词标题生成Service业务层处理
 * 
 * @author xialang
 * @date 2023-05-30
 */
@Service
public class DictKeywordTitleServiceImpl implements IDictKeywordTitleService 
{
    @Autowired
    private DictKeywordTitleMapper dictKeywordTitleMapper;

    /**
     * 查询关键词标题生成
     * 
     * @param id 关键词标题生成主键
     * @return 关键词标题生成
     */
    @Override
    public DictKeywordTitle selectDictKeywordTitleById(Long id)
    {
        return dictKeywordTitleMapper.selectDictKeywordTitleById(id);
    }

    /**
     * 查询关键词标题生成列表
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 关键词标题生成
     */
    @Override
    public List<DictKeywordTitle> selectDictKeywordTitleList(DictKeywordTitle dictKeywordTitle)
    {

        return dictKeywordTitleMapper.selectDictKeywordTitleList(dictKeywordTitle);
    }
    /**
     * 新增关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    @Override
    public int insertDictKeywordTitle(DictKeywordTitle dictKeywordTitle)
    {
        dictKeywordTitle.setCreateTime(DateUtils.getNowDate());
        return dictKeywordTitleMapper.insertDictKeywordTitle(dictKeywordTitle);
    }

    /**
     * 修改关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    @Override
    public int updateDictKeywordTitle(DictKeywordTitle dictKeywordTitle)
    {
        dictKeywordTitle.setUpdateTime(DateUtils.getNowDate());
        return dictKeywordTitleMapper.updateDictKeywordTitle(dictKeywordTitle);
    }

    /**
     * 批量删除关键词标题生成
     * 
     * @param ids 需要删除的关键词标题生成主键
     * @return 结果
     */
    @Override
    public int deleteDictKeywordTitleByIds(Long[] ids)
    {
        return dictKeywordTitleMapper.deleteDictKeywordTitleByIds(ids);
    }

    /**
     * 删除关键词标题生成信息
     * 
     * @param id 关键词标题生成主键
     * @return 结果
     */
    @Override
    public int deleteDictKeywordTitleById(Long id)
    {
        return dictKeywordTitleMapper.deleteDictKeywordTitleById(id);
    }
}
