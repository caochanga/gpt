package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OpenaiMsgGroups;

/**
 * 存储对话组的Service接口
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
public interface IOpenaiMsgGroupsService 
{
    /**
     * 查询存储对话组的
     * 
     * @param id 存储对话组的主键
     * @return 存储对话组的
     */
    public OpenaiMsgGroups selectOpenaiMsgGroupsById(Long id);

    /**
     * 查询存储对话组的列表
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 存储对话组的集合
     */
    public List<OpenaiMsgGroups> selectOpenaiMsgGroupsList(OpenaiMsgGroups openaiMsgGroups);

    /**
     * 新增存储对话组的
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 结果
     */
    public int insertOpenaiMsgGroups(OpenaiMsgGroups openaiMsgGroups);

    /**
     * 修改存储对话组的
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 结果
     */
    public int updateOpenaiMsgGroups(OpenaiMsgGroups openaiMsgGroups);

    /**
     * 批量删除存储对话组的
     * 
     * @param ids 需要删除的存储对话组的主键集合
     * @return 结果
     */
    public int deleteOpenaiMsgGroupsByIds(Long[] ids);

    /**
     * 删除存储对话组的信息
     * 
     * @param id 存储对话组的主键
     * @return 结果
     */
    public int deleteOpenaiMsgGroupsById(Long id);
}
