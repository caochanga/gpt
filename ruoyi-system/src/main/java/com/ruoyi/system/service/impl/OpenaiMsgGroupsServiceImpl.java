package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OpenaiMsgGroupsMapper;
import com.ruoyi.system.domain.OpenaiMsgGroups;
import com.ruoyi.system.service.IOpenaiMsgGroupsService;

/**
 * 存储对话组的Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
@Service
public class OpenaiMsgGroupsServiceImpl implements IOpenaiMsgGroupsService 
{
    @Autowired
    private OpenaiMsgGroupsMapper openaiMsgGroupsMapper;

    /**
     * 查询存储对话组的
     * 
     * @param id 存储对话组的主键
     * @return 存储对话组的
     */
    @Override
    public OpenaiMsgGroups selectOpenaiMsgGroupsById(Long id)
    {
        return openaiMsgGroupsMapper.selectOpenaiMsgGroupsById(id);
    }

    /**
     * 查询存储对话组的列表
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 存储对话组的
     */
    @Override
    public List<OpenaiMsgGroups> selectOpenaiMsgGroupsList(OpenaiMsgGroups openaiMsgGroups)
    {
        return openaiMsgGroupsMapper.selectOpenaiMsgGroupsList(openaiMsgGroups);
    }

    /**
     * 新增存储对话组的
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 结果
     */
    @Override
    public int insertOpenaiMsgGroups(OpenaiMsgGroups openaiMsgGroups)
    {
        openaiMsgGroups.setCreateTime(DateUtils.getNowDate());
        return openaiMsgGroupsMapper.insertOpenaiMsgGroups(openaiMsgGroups);
    }

    /**
     * 修改存储对话组的
     * 
     * @param openaiMsgGroups 存储对话组的
     * @return 结果
     */
    @Override
    public int updateOpenaiMsgGroups(OpenaiMsgGroups openaiMsgGroups)
    {
        openaiMsgGroups.setUpdateTime(DateUtils.getNowDate());
        return openaiMsgGroupsMapper.updateOpenaiMsgGroups(openaiMsgGroups);
    }

    /**
     * 批量删除存储对话组的
     * 
     * @param ids 需要删除的存储对话组的主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiMsgGroupsByIds(Long[] ids)
    {
        return openaiMsgGroupsMapper.deleteOpenaiMsgGroupsByIds(ids);
    }

    /**
     * 删除存储对话组的信息
     * 
     * @param id 存储对话组的主键
     * @return 结果
     */
    @Override
    public int deleteOpenaiMsgGroupsById(Long id)
    {
        return openaiMsgGroupsMapper.deleteOpenaiMsgGroupsById(id);
    }
}
