package com.ruoyi.system.openai;

import com.ruoyi.common.utils.ChatResponse;
import com.ruoyi.system.domain.dto.ChatRequest;
import com.unfbx.chatgpt.entity.chat.Message;

import java.util.List;

public interface RequestOpenAi {
    /**
     * 客户端发送消息到服务端
     * @param uid
     * @param chatRequest
     */
    ChatResponse sseChat(String uid, ChatRequest chatRequest);

    /**
     * @param chatRequest 请求参数，放个类方便后期修改
     * @return
     */
    List<Message> send(ChatRequest chatRequest);

    List<Message> sendGPT(List<Message> msgList);
}
