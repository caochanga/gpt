package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 存储对话消息的对象 openai_msg
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
public class OpenaiMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 每条消息的唯一标识符 */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Integer userId;

    /** 消息所属的对话组ID */
    @Excel(name = "消息所属的对话组ID")
    private Long openaiMsgGroupsId;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String messageContent;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setOpenaiMsgGroupsId(Long openaiMsgGroupsId) 
    {
        this.openaiMsgGroupsId = openaiMsgGroupsId;
    }

    public Long getOpenaiMsgGroupsId() 
    {
        return openaiMsgGroupsId;
    }
    public void setMessageContent(String messageContent) 
    {
        this.messageContent = messageContent;
    }

    public String getMessageContent() 
    {
        return messageContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("openaiMsgGroupsId", getOpenaiMsgGroupsId())
            .append("messageContent", getMessageContent())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
