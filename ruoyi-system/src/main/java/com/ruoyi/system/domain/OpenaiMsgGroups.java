package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 存储对话组的对象 openai_msg_groups
 * 
 * @author ruoyi
 * @date 2023-05-31
 */
public class OpenaiMsgGroups extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 对话组的唯一标识符 */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Integer userId;

    /** 对话组的名称 */
    @Excel(name = "对话组的名称")
    private String groupsName;

    /** 分类，1、系统，2、用户 */
    @Excel(name = "分类，1、系统，2、用户")
    private String groupsType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setGroupsName(String groupsName) 
    {
        this.groupsName = groupsName;
    }

    public String getGroupsName() 
    {
        return groupsName;
    }
    public void setGroupsType(String groupsType) 
    {
        this.groupsType = groupsType;
    }

    public String getGroupsType() 
    {
        return groupsType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("groupsName", getGroupsName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("groupsType", getGroupsType())
            .toString();
    }
}
