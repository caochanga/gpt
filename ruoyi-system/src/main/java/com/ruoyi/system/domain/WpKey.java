package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户网站管理对象 wp_key
 * 
 * @author ruoyi
 * @date 2023-05-26
 */
public class WpKey extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** sys_user_id */
    @Excel(name = "sys_user_id")
    private Long userId;

    /** 网站域名路径 */
    @Excel(name = "网站域名路径")
    private String siteUrl;

    /** 秘钥 */
    @Excel(name = "秘钥")
    private String apiToken;

    /** 网站名称 */
    @Excel(name = "网站名称")
    private String siteName;

    /** 网站类型 1、wordpres 2、其他 */
    @Excel(name = "网站类型 1、wordpres 2、其他")
    private Integer type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSiteUrl(String siteUrl) 
    {
        this.siteUrl = siteUrl;
    }

    public String getSiteUrl() 
    {
        return siteUrl;
    }
    public void setApiToken(String apiToken) 
    {
        this.apiToken = apiToken;
    }

    public String getApiToken() 
    {
        return apiToken;
    }
    public void setSiteName(String siteName) 
    {
        this.siteName = siteName;
    }

    public String getSiteName() 
    {
        return siteName;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("siteUrl", getSiteUrl())
            .append("apiToken", getApiToken())
            .append("siteName", getSiteName())
            .append("type", getType())
            .toString();
    }
}
