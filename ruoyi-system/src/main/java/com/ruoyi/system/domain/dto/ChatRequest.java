package com.ruoyi.system.domain.dto;
/**
 * 描述：
 *
 * @author https:www.unfbx.com
 * @sine 2023-04-08
 */
public class ChatRequest {
    /**
     * 客户端发送的问题参数
     */
    private String msg;

    /**
     * code 写文章还是写标题，用数值来区分 暂未定义值后期再去定义
     * 1 写标题
     * 2 写文章
     */
    private Integer code;

    /**
     * 生成多少个
     */
    private Integer amount;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
