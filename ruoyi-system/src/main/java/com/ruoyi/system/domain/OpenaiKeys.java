package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * OpenAI密钥存储对象 openai_keys
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
public class OpenaiKeys extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** OpenAI API密钥 */
    @Excel(name = "OpenAI API密钥")
    private String openaiApiKey;

    /** 余额 */
    @Excel(name = "余额")
    private BigDecimal balance;

    /** 账号状态（1-正常、2-余额为0、3-封号） */
    @Excel(name = "账号状态", readConverterExp = "1=-正常、2-余额为0、3-封号")
    private String status;

    /** 版本（1、GPT3.5，2、GPT4） */
    @Excel(name = "版本", readConverterExp = "1=、GPT3.5，2、GPT4")
    private String versions;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setOpenaiApiKey(String openaiApiKey) 
    {
        this.openaiApiKey = openaiApiKey;
    }

    public String getOpenaiApiKey() 
    {
        return openaiApiKey;
    }
    public void setBalance(BigDecimal balance) 
    {
        this.balance = balance;
    }

    public BigDecimal getBalance() 
    {
        return balance;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setVersions(String versions) 
    {
        this.versions = versions;
    }

    public String getVersions() 
    {
        return versions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("openaiApiKey", getOpenaiApiKey())
            .append("balance", getBalance())
            .append("status", getStatus())
            .append("versions", getVersions())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
