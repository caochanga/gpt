package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * openAI使用地址对象 openai_url
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
public class OpenaiUrl extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 网址路径 */
    @Excel(name = "网址路径")
    private String openUrl;

    /** 有效状态. 1：有效；0：无效 */
    @Excel(name = "有效状态. 1：有效；0：无效")
    private Integer validStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOpenUrl(String openUrl) 
    {
        this.openUrl = openUrl;
    }

    public String getOpenUrl() 
    {
        return openUrl;
    }
    public void setValidStatus(Integer validStatus) 
    {
        this.validStatus = validStatus;
    }

    public Integer getValidStatus() 
    {
        return validStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("openUrl", getOpenUrl())
            .append("validStatus", getValidStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
