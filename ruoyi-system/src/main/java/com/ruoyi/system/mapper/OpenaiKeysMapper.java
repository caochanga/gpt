package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.OpenaiKeys;

import java.util.List;

/**
 * OpenAI密钥存储Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-29
 */
public interface OpenaiKeysMapper 
{
    /**
     * 查询OpenAI密钥存储
     * 
     * @param id OpenAI密钥存储主键
     * @return OpenAI密钥存储
     */
    public OpenaiKeys selectOpenaiKeysById(Long id);

    /**
     * 查询OpenAI密钥存储列表
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return OpenAI密钥存储集合
     */
    public List<OpenaiKeys> selectOpenaiKeysList(OpenaiKeys openaiKeys);

    /**
     * 新增OpenAI密钥存储
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return 结果
     */
    public int insertOpenaiKeys(OpenaiKeys openaiKeys);

    /**
     * 修改OpenAI密钥存储
     * 
     * @param openaiKeys OpenAI密钥存储
     * @return 结果
     */
    public int updateOpenaiKeys(OpenaiKeys openaiKeys);

    /**
     * 删除OpenAI密钥存储
     * 
     * @param id OpenAI密钥存储主键
     * @return 结果
     */
    public int deleteOpenaiKeysById(Long id);

    /**
     * 批量删除OpenAI密钥存储
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOpenaiKeysByIds(Long[] ids);

    List<String> getApiList();
}
