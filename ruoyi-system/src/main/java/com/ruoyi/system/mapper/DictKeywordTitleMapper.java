package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DictKeywordTitle;

import java.util.List;

/**
 * 关键词标题生成Mapper接口
 * 
 * @author xialang
 * @date 2023-05-30
 */
public interface DictKeywordTitleMapper 
{
    /**
     * 查询关键词标题生成
     * 
     * @param id 关键词标题生成主键
     * @return 关键词标题生成
     */
    public DictKeywordTitle selectDictKeywordTitleById(Long id);

    /**
     * 查询关键词标题生成列表
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 关键词标题生成集合
     */
    public List<DictKeywordTitle> selectDictKeywordTitleList(DictKeywordTitle dictKeywordTitle);

    /**
     * 新增关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    public int insertDictKeywordTitle(DictKeywordTitle dictKeywordTitle);

    /**
     * 修改关键词标题生成
     * 
     * @param dictKeywordTitle 关键词标题生成
     * @return 结果
     */
    public int updateDictKeywordTitle(DictKeywordTitle dictKeywordTitle);

    /**
     * 删除关键词标题生成
     * 
     * @param id 关键词标题生成主键
     * @return 结果
     */
    public int deleteDictKeywordTitleById(Long id);

    /**
     * 批量删除关键词标题生成
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDictKeywordTitleByIds(Long[] ids);
}
