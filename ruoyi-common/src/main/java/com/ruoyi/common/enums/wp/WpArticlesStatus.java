package com.ruoyi.common.enums.wp;

public enum WpArticlesStatus {
    TO_ISSUE(1, "待发布"),
    EXECUTED(2, "已发布"),
    ;

    private final Integer code;
    private final String info;

    WpArticlesStatus(Integer code,String  info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
