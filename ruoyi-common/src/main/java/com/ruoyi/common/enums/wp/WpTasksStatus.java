package com.ruoyi.common.enums.wp;

/**
 * (1：未执行，2、执行中、3、掉单、4、已完成)
 * */
public enum WpTasksStatus
{
    TO_EXECUTE(1, "未执行"),
    IN_EXECUTE(2, "执行中"),
    QUIT(3, "删除"),
    SUCCESS(4, "已完成"),
    FILE(999, "脏数据"),;

    private final Integer code;
    private final String info;

    WpTasksStatus(Integer code,String  info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
