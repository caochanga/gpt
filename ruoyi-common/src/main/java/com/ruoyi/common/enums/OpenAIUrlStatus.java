package com.ruoyi.common.enums;

public enum OpenAIUrlStatus {
    OK(1, "有效"),
    INVALID(0, "无效");

    private final Integer code;
    private final String info;

    OpenAIUrlStatus(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}