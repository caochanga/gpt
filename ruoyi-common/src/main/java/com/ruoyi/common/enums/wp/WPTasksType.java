package com.ruoyi.common.enums.wp;
/**
 * 任务类型(1、写标题，2、写文章，3、用户自定义，0、其他)
 * */
public enum WPTasksType
{
    ADD_TITLE(1, "写标题"),
    ADD_ARTICLE(2, "写文章"),
    ADD_USER(3, "删除"),
    OUTER(0, "其他"),
    FILE(999, "脏数据"),;

    private final Integer code;
    private final String info;

    WPTasksType(Integer code,String  info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
