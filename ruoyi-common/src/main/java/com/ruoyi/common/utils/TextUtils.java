package com.ruoyi.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextUtils {

    public static String reformatText(String inputText) {
        String trimmedText = String.join(" ", Arrays.asList(inputText.split("\\s+")));
        if (inputText.contains(" ")) {
            trimmedText = trimmedText.replace(" ", "<br>");
        }
        List<String> splitCharacters = new ArrayList<>(Arrays.asList("。", "！", "？", "?",";"));
        return splitTextByCharacters(trimmedText, splitCharacters);
    }

    private static String splitTextByCharacters(String text, List<String> splitCharacters) {
        String newLineReplacement = "<br>";
        for (String splitCharacter : splitCharacters) {
            text = text.replace(splitCharacter, splitCharacter + newLineReplacement);
        }
        return text;
    }

}
