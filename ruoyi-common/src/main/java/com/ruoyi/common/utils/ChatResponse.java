package com.ruoyi.common.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述：
 *
 * @author https:www.unfbx.com
 * @sine 2023-04-08
 */
public class ChatResponse {
    /**
     * 问题消耗tokens
     */
    @JsonProperty("question_tokens")
    private long questionTokens = 0;


    public long getQuestionTokens() {
        return questionTokens;
    }

    public void setQuestionTokens(long questionTokens) {
        this.questionTokens = questionTokens;
    }
}
